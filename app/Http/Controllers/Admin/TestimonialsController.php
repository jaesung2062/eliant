<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::orderBy('index')->get();

        return view('admin.testimonials.index', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'quote' => 'required',
            'person' => 'required',
            'company' => 'required',
        ]);

        $testimonial = Testimonial::create($request->only('quote', 'person', 'company'));

        if ($file = $request->file('avatar')) { //person_image
            $testimonial->attachPersonalImage($file);
        }

        session()->flash('success', 'Created testimonial.');

        return redirect("/admin/testimonials/$testimonial->id/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('admin.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        $this->validate($request, [
            'quote' => 'required',
            'person' => 'required',
            'company' => 'required',
        ]);

        $testimonial->update($request->only('quote', 'person', 'company'));

        if ($file = $request->file('person_image')) {
            $testimonial->attachPersonalImage($file);
        }

        session()->flash('success', 'Updated testimonial.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        $testimonial->delete();

        session()->flash('success', 'Deleted testimonial.');

        return response('good');
    }

    public function reorder(Request $request)
    {
        $index = 0;

        foreach ($request->order as $id) {
            Testimonial::find($id)->update(compact('index'));

            $index++;
        }

        return response('good');
    }
}
