<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\WinnersList;
use Illuminate\Http\Request;

class WinnersListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = WinnersList::all();

        return view('admin.winners-list.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.winners-list.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'winners' => 'required',
            'class' => 'required'
        ]);

        $list = WinnersList::create($request->only('name', 'winners', 'type', 'description', 'class'));

        session()->flash('success', 'Created list.');

        return redirect("/admin/awards/$list->id/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WinnersList  $winnersList
     * @return \Illuminate\Http\Response
     */
    public function show(WinnersList $winnersList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = WinnersList::find($id);

        return view('admin.winners-list.edit', compact('list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WinnersList  $winnersList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WinnersList $winnersList)
    {
        $this->validate($request, [
            'name' => 'required',
            'winners' => 'required',
            'class' => 'required'
        ]);

        $winnersList->update($request->only('name', 'winners', 'type', 'description', 'class'));

        session()->flash('success', 'Successfully updated winners list.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WinnersList  $winnersList
     * @return \Illuminate\Http\Response
     */
    public function destroy(WinnersList $winnersList)
    {
        //
    }
}
