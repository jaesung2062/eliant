<?php

namespace App\Http\Controllers;

use Agent;
use App\Mail\ContactSubmission;
use App\Mail\OnsiteSubmission;
use App\Mail\RatetradeSubmission;
use App\Mail\HBCSubmission;
use App\Mail\Subscribed;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function show()
    {
        $meta = [
            'title' => 'Contact Eliant | 866-755-8199',
            'description' => 'We\'re ready create your next questionnaires, interpret your customers’ feedback and create an Action Plan for delivering extraordinary customer experience.',
        ];

        return view((Agent::isMobile() ? 'mobile' : 'pages').'.contact', compact('meta'));
        //return view('pages.contact', compact('meta'));
    }

    public function submit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'number' => 'required',
        ]);

        \Mail::to(env('MAIL_USERNAME', 'nick@wbrandstudio.com'))->send(new ContactSubmission($request->all()));



        session()->flash('success', 'Successfully sent message!');

        return redirect('/');
    }

    public function onsite(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'number' => 'required',
        ]);

        \Mail::to(env('MAIL_USERNAME', 'nick@wbrandstudio.com'))->send(new OnsiteSubmission($request->all()));



        session()->flash('success', 'Successfully sent message!');

        return redirect('/');
    }

    public function ratetrades(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'number' => 'required',
        ]);

        \Mail::to(env('MAIL_USERNAME', 'nick@wbrandstudio.com'))->send(new RatetradeSubmission($request->all()));



        session()->flash('success', 'Successfully sent message!');

        return redirect('/');
    }

    public function sub(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'number' => 'required',
        ]);

        \Mail::to(env('MAIL_USERNAME', 'nick@wbrandstudio.com'))->send(new HBCSubmission($request->all()));



        session()->flash('success', 'Successfully sent message!');

        return redirect('/');
    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'submail' => 'required|email',

        ]);

        \Mail::to(env('MAIL_USERNAME', 'nick@wbrandstudio.com'))->send(new Subscribed($request->all()));

        session()->flash('success', 'Successfully sent message!');

        return redirect('/');
    }
}
