<?php

namespace App\Http\Controllers;

use App\Mail\SponsorForm;
use App\Testimonial;
use App\WinnersList;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        return $this->getView('home');
    }

    public function about()
    {
        return $this->getView('about');
    }

    public function landingOne()
    {
        return $this->getView('landingOne');
    }

    public function landingTwo()
    {
        return $this->getView('landingTwo');
    }

    public function landingThree()
    {
        return $this->getView('landingThree');
    }

    public function onsite()
    {
        return $this->getView('onsite');
    }
    public function ratetrades()
    {
        return $this->getView('ratetrades');
    }

    public function cem()
    {
        return redirect()->away('http://cem.eliant.com');

    }

    public function hbc()
    {
        return $this->getView('hbclanding');
    }

    public function surveys()
    {
        return $this->getView('surveys');
    }

    public function services()
    {
        return $this->getView('services');
    }

    public function sponsors()
    {
        return $this->getView('sponsors');
    }

    public function postSponsors(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        \Mail::to(env('CONTACT_EMAIL', 'jeff@wbrandstudio.com'))->send(new SponsorForm($request->all()));

        session()->flash('success', 'Successfully sent message!');

        return redirect()->back();
    }

    public function testimonials()
    {
        $testimonials = Testimonial::orderBy('index')->get();

        return $this->getView('testimonials', compact('testimonials'));
    }

    public function choiceAwards()
    {
        return $this->getView('choice-awards');
    }

    public function choiceAwardsWinners()
    {

       // return $this->getView('choice-awards-winners');
        $builderWinnersLists = WinnersList::where('class', 'builder')->get();
        $individualWinnersLists = WinnersList::where('class', 'individual')->get();

        return $this->getView('choice-awards-winners', compact('builderWinnersLists', 'individualWinnersLists'));
    }

    public function faq()
    {
        $faqs = [
            [
                'question' => 'Should I conduct my own surveys?',
                'answer' => 'Our research with prosective clients repeatedly confirms that almost all these firms’ internally
administered customer surveys suffer from low response rates - which invalidate the survey
ratings. Typical response rates (i.e. customer participation levels) are about 20-35% at best.
Moreover, the survey questions are all too often poorly written and not chosen with the appropriate
degree of statistical relevance. This process does not allow you to compare yourselves against other
similar firms/competitors.'
            ],
            [
                'question' => 'Are phone surveys good?',
                'answer' => 'Before we answer that question, here is a question for YOU: Do you like to be called at night to
answer survey questions on your phone?
You’re not alone. A recent research study found that 92% of home owners said they would not
participate in random surveys conducted by phone.
That’s why our answer to “Are phone surveys good?” is most often “NO”!
Eliant terminated its phone surveying process years ago when multiple university studies confirmed
that phone surveys are terribly biased with positively-skewed responses (i.e. false positives). Why?
Most phone surveys are conducted when customers are at home at the end of the work day. Your
interviewers would have to interrupt your customers’ family time and/or dinner...yech. Customers are
primarily motivated to just want to get you off the phone, so they give overly positive responses to
avoid having to explain the reasons for their dissatisfaction to the interviewer.  You are also
depending on this minimum-wage interviewer to interpret your customers’ comments and choose the
ones to enter into the record.
The only time phone surveys might by worthwhile is for very short surveys of 3-4 questions taking no
more than 1-2 minutes.'
            ],
            [
                'question' => 'Is longer better than shorter?',
                'answer' => 'Shorter surveys are tough to design, but they are generally better. Why? Higher response rates.
These increased responses help to validate the ratings.  We need to make sure your survey is long
enough to get you the answers, improvement ideas, and information you need, but short enough to
keep your response rates high. 
This is one of the reasons why Eliant is viewed as the Gold Standard in quality questionnaires:
Eliant’s questionnaires are sometimes lengthy, but by using question-design techniques derived
from university and psychological research, we still yield a 70% average response level.'
            ],
            [
                'question' => 'Why use Eliant?',
                'answer' => 'Our leadership team has experience in psychology, survey design, home building, apartment
management, mortgages, online marketing, and technology.  We are the oldest (and original) survey
company for the real estate industry (1984).  Our CEO is a clinical psychologist who spent years
creating psychological testing instruments; this guy knows how to ask the right questions!
Our Dashboard Reports Portal is also nationally acclaimed as the easiest-to- use reporting tool in our
industry!

Most importantly, we are NOT just a survey company. Our mission is to assist our clients (in any
industry) to collect, analyze, interpret, and then CORRECTLY USE customers’ feedback to improve
the customer-experience and sales emanating from customer referrals. Our experienced team of
Client Success Managers are tasked with guiding our clients to the correct solutions.'
            ],
            [
                'question' => 'Should I be scared about benchmarking to my
competitors?',
                'answer' => 'No, our “Ranking Report” comparisons confidentially show how you are doing against our average
client or the Top-10 clients by name.  Your results aren’t shared with other builders unless you are a
Top-10 performer...and that is something you will be proud to share!'
            ],
            [
                'question' => 'Why are there three separate surveys for rateHOME
during the buyer’s first year in the home?',
                'answer' => 'Our three surveys ask homeowners to evaluate their experience at three different milestones:
<br>1. The “Move-In Evaluation” covers the sales, design, lender, construction, and move in
experience
<br>2. The “Mid-Year Customer Service Audit” covers warranty work. 
<br>3. The “Year-End Home Quality Assessment” covers warranty and lots of quality and design
feedback for the purpose of continuous improvement and risk reduction.'
            ],
            [
                'question' => 'Can I add surveys to my process?',
                'answer' => 'Yes, we have a variety of other tools used by builders, lenders, design firms, property management
companies, etc. This includes many types of short “snapshot” surveys administered during specific
points of any customer process: Mid-Construction Surveys; Design Center Survey; Lender
Certification Evaluation; RateTrades TM ; RateBuilder TM , etc.'
            ],
            [
                'question' => 'Can I do custom surveys or reports?',
                'answer' => 'Of course! We do this all the time.'
            ],
            [
                'question' => 'Can you send surveys in the language of my customer?',
                'answer' => 'Yes, our Benchmark surveys are offered in 16 languages.'
            ],
            [
                'question' => 'Why is a survey’s question wording & design important?',
                'answer' => 'The question’s wording &amp; design can bias a customer to answer more negatively or positively or give
you responses that may be misleading.  Survey and question design is a science.  Our CEO is a
trained psychologist who, in his early career, spent years creating psychological diagnostic tools
including tests and questionnaires.'
            ],
            [
                'question' => 'What is the structure of Eliant training?',
                'answer' => 'Our training is delivered in a variety of formats. Our standard training process includes a
complimentary on-line library of over 100 articles, training modules, lists of best practices, and top
tips from our highest rated clients.
We offer complimentary 30-60 minute webinar/phone calls to train your team how to properly
introduce the surveying process to their customers.  Or how to access and interpret the information
in the Dashboard portal. We can deliver live face-to- face departmental training session for 1-4
hours.  Our company president, an ex-executive for a large public builder for 8+ years, delivers fee-
based custom action-planning workshops for 4-8 hours for the entire management team or all
customer-facing team members.'
            ],
            [
                'question' => 'What does Eliant Certification mean?',
                'answer' => '“Eliant Certified” is a designation earned only by those loan officers, sales agents, superintendents,
design consultants, customer service representatives, and other customer-facing people who score
exceptionally well on their customer evaluations.
The Certification program: We identify those key behaviors which drive your customers’ satisfaction
and willingness to refer a friend. We ask customers to evaluate your team’s ability to perform these
key behaviors. We categorize your reps by aggregated survey score: Bronze, Silver, or Gold
Certification. We offer training for reps struggling to get certified or who want to improve their
certification levels.'
            ],
            [
                'question' => 'Why is an assigned “Client Success Manager” important?',
                'answer' => 'Eliant offers more than customer surveys. We assist your efforts to improve your customers’
satisfaction and willingness to refer their friends.
With one of our “Client Success Managers” assigned to your company, you know you have one
place to get all your questions answered.  You have a partner reviewing your results and making
recommendations. You are able to lean on an Eliant Client Success Manager to design custom
surveys, reports, and training workshops.'
            ],
            [
                'question' => 'Why is the 5-month Customer Service Audit important?',
                'answer' => 'It’s the only survey focused on the warranty process.  It’s the first opportunity to get a picture of what
customer service looks like from your customers’ perspective. It gives you a heads up for buyers
having issues with warranty before the end of the 1 year mark.'
            ],
            [
                'question' => 'What is best way to use 10-month survey data?',
                'answer' => 'Hold regular meetings with the people on your team who help design floorplans, choose options and
standard features and who want to improve the customer experience.  You need to review the voice
of the customer related to your home designs and what you are installing in these homes.  That is
the only way to continuously improve and not continue building errors over and over again.'
            ],
            [
                'question' => 'How much does it cost?',
                'answer' => 'Hit us up for a demo so we can get you your pricing.'
            ],
            [
                'question' => 'Can I set up a demo?',
                'answer' => 'Yes.  Click <a href="/contact">HERE</a> to set up a demo! Thank you!'
            ],
//            [
//                'question' => 'What about a trial period, we are new to surveys?',
//                'answer' => 'We do run specials sometimes offering a 90 day trial so you can see how valuable it is to get continuous improvement feedback and comments from your customers.  Please contact us to see what specials we are running at this time.'
//            ],
        ];

        return $this->getView('faq', compact('faqs'));
    }
}
