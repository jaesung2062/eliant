<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $meta = [
            'home' => [
                'title' => 'Eliant | Nationwide Customer Survey Company',
                'description' => 'Anyone can ask questions, but asking the RIGHT ones is what gives us an edge in understanding your customers’ experience.',
                'keywords' => 'market research, customer satisfaction',
                'author' => 'W Brand Studio'
            ],
            'about' => [
                'title' => 'Eliant | We Ask the Right Questions, Not Just Questions',
                'description' => 'We\'ve been conducting intensive customer research and evaluations nationwide for over 30 years, and more than 2.5 million consumer surveys.',
                'keywords' => 'industry survey, client engagement, customer success'
            ],
            'choice-awards' => [
                'title' => 'The Home Buyers’ Choice Awards from Eliant',
                'description' => 'Did your customers build you up as the best? The Home Buyers’ Choice Awards honor new home builders who deliver the best experience to their customers.',
                'keywords' => 'home buyer award, builder award, eliant home buyer'
            ],
            'contact' => [
                'title' => 'Contact Eliant | (866) 755-8199',
                'description' => 'We\'re ready create your next questionnaires, interpret your customers’ feedback and create an Action Plan for delivering extraordinary customer experience.',
                'keywords' => 'contact eliant, eliant office'
            ],
            'faq' => [
                'title' => 'Eliant Frequently Asked Questions',
                'description' => 'Find answers to frequently asked questions about our customer satisfaction surveys for the real estate and other key industries.',
                'keywords' => 'faq, customer questions'
            ],
            'services' => [
                'title' => 'Eliant Services & Training | The Answer To Your Success',
                'description' => 'We help provide clients with valuable improvement insights, certification and training that offers a deeper look into what your customers are thinking.',
                'keywords' => 'market research, customer satisfaction '
            ],
            'surveys' => [
                'title' => 'Eliant Surveys For Any Industry',
                'description' => 'No matter what field you’re in, we collect customer evaluations which provide the right answers to grow your business in ways you never thought possible.',
                'keywords' => 'customer surveys, market research, customer satisfaction'
            ],
            'testimonials' => [
                'title' => 'Eliant Client Testimonials | See Our Clients Answers',
                'description' => 'See what some of our valuable clients say about how the right survey makes all the difference when it comes to improving customer service.',
                'keywords' => 'customer experience, our clients'
            ],
            'blog.index' => [
                'title' => 'Eliant Blog | Collection Of Interesting & Informative Articles',
                'description' => 'Keep up with our growing collection of interesting and informative articles on incredible insights and the right answers customers satisfaction success.',
                'keywords' => 'blog, articles, press releases'
            ],
        ];

        foreach ($meta as $page => $data) {
            foreach(['pages', 'mobile'] as $type) {
                \View::composer("$type.$page", function ($view) use ($data) {
                    $view->with('meta', $data);
                });
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
