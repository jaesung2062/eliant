<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Testimonial extends Model
{
    protected $fillable = [
        'quote', 'person', 'company', 'index',
    ];

    public function attachPersonalImage(UploadedFile $uploadedFile)
    {
        $file = new \Illuminate\Http\File($uploadedFile->getRealPath());
        Storage::putFileAs("testimonials/$this->id", $file, 'person_image.jpg');
    }

    public function attachCompanyImage(UploadedFile $uploadedFile)
    {
        $file = new \Illuminate\Http\File($uploadedFile->getRealPath());
        Storage::putFileAs("testimonials/$this->id", $file, 'company_image.jpg');
    }

    public function getPersonImagePath()
    {
        $imagePath = storage_path("app/public/testimonials/$this->id/person_image.jpg");

        if (File::exists($imagePath)) {
            return Storage::url("testimonials/$this->id/person_image.jpg");
        }

        return '/images/person_image_placeholder.jpg';
    }

    public function getCompanyImagePath()
    {
        $imagePath = storage_path("app/public/testimonials/$this->id/company_image.jpg");

        if (File::exists($imagePath)) {
            return Storage::url("testimonials/$this->id/company_image.jpg");
        }

        return false;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function (Testimonial $testimonial) {
            $testimonial->index = Testimonial::max('index') + 1;
        });
    }
}
