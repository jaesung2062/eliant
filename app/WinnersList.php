<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WinnersList extends Model
{
    protected $fillable = [
        'name', 'winners', 'type', 'description', 'class', 'is_alphabetical',
    ];

    public static $types = [
        'simple', 'ordered', 'ordered-categorized',
    ];

    public function setWinnersAttribute($winners)
    {
        return $this->attributes['winners'] = json_encode($winners);
    }

    public function getWinnersAttribute($winners)
    {
        return json_decode($winners, true);
    }
}
