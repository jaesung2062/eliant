<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Testimonial::class, function (Faker\Generator $faker) {
    return [
        'quote' => $faker->paragraph,
        'person' => $faker->name,
        'company' => $faker->company,
    ];
});


$factory->define(App\WinnersList::class, function (Faker\Generator $faker) {
    $type = $faker->randomElement(\App\WinnersList::$types);

    switch ($type) {
        case 'simple';
        case 'ordered';
            break;
        case 'ordered-categorized';
            break;
    }

    return [
        'name' => $faker->name,
        'type' => $type,
        'winners' => $faker->company,
    ];
});
