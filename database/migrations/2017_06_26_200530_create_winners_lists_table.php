<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinnersListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winners_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('winners');
            $table->text('description');
            $table->string('type');
            $table->string('class');
            $table->boolean('is_alphabetical')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winners_lists');
    }
}
