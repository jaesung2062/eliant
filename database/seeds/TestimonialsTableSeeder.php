<?php

use Illuminate\Database\Seeder;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getTestimonials() as $i => $testimonial) {
            $testimonial['index'] = $i;

            /* @var $model \App\Testimonial */
            $model = \App\Testimonial::create(array_only($testimonial, ['quote', 'person', 'company', 'index']));

            if ($testimonial['person_image']) {
                $fileName = $testimonial['person_image'];
                $imagePath = __DIR__.'/testimonial-images/'.$fileName;
                $uploadedFile = new \Illuminate\Http\UploadedFile($imagePath, $fileName);

                $model->attachPersonalImage($uploadedFile);
            }

            if ($testimonial['company_image']) {
                $fileName = $testimonial['company_image'];
                $imagePath = __DIR__.'/testimonial-images/'.$fileName;
                $uploadedFile = new \Illuminate\Http\UploadedFile($imagePath, $fileName);

                $model->attachCompanyImage($uploadedFile);
            }
        }
    }

    protected function getTestimonials()
    {
        return [
            [
                'quote' => 'The best part (of working with Eliant) is our ongoing personal contact with Bob, Jex and other Eliant staff. Bottom Line: 34% of our sales last year were from referrals."',
                'person' => 'Jeffrey M. Hack, President & CEO',
                'person_image' => '',
                'company' => 'Van Daele Homes, LLC',
                'company_image' => 'van-daele-homes-person.jpg'
            ],
            [
                'quote' => 'Eliant provided us with extremely creative and effective recommendations for improving our customers’ experience. Our #1 ranking on Eliant surveys and 35% of sales from referrals is the result."',
                'person' => 'Larry Webb, CEO',
                'person_image' => 'larry-web.jpg',
                'company' => 'The New Home Company',
                'company_image' => 'new-home-company.jpg',
            ],
            [
                'quote' => 'For 30 years, William Lyon Homes has proudly worked with the Eliant team. We have seen continuous improvement in our customers’ overall experience and an increase in sales from customer referrals."',
                'person' => 'Brian W. Doyle, President',
                'person_image' => 'brian-doyle.jpg',
                'company' => 'William Lyon Homes - California',
                'company_image' => 'william-lyon-homes.jpg',
            ],
            [
                'quote' => 'Our greatest source of new sales: Evangelical Robson Community homebuyers! Thank you, Eliant!"',
                'person' => 'Chris Harrison, COO',
                'person_image' => 'chris-harrison.jpg',
                'company' => 'Robson Communities',
                'company_image' => 'robson-resort-communities.jpg',
            ],
            [
                'quote' => 'The Eliant program has been instrumental in helping us grow a company culture that is focused on delivering a world-class home building experience for our customers."',
                'person' => 'Chris Harrison, COO',
                'person_image' => 'chris-harrison.jpg',
                'company' => 'Robson Communities',
                'company_image' => 'robson-resort-communities.jpg',
            ],
            [
                'quote' => 'We have used your company for over a decade to measure the satisfaction of our customers. We believe meeting and exceeding the expectations of one\'s customers is at the core of a company\'s survival. What better way to measure this than to solicit input from your own customers? The survey results coupled with benchmarking against other builders is used to true MBK\'s processes and set the standards.I give Eliant the highest recommendation and the surveys you produce are not only extremely helpful but essential to the long term survival of MBK Homes."',
                'person' => 'Tim Kane, President - Irvine',
                'person_image' => 'tim-kane.jpg',
                'company' => 'MBK Homes',
                'company_image' => 'mbk-homes.jpg',
            ],
            [
                'quote' => 'Eliant has allowed us to maintain a customer-centric culture, which has increased referral sales for our builder and realtor clients!"',
                'person' => 'Dan Hanson, Executive VP of National Production',
                'person_image' => 'dan-hanson.jpg',
                'company' => 'imortgage',
                'company_image' => 'imortage.jpg',
            ],
            [
                'quote' => 'In 2013, 33 % of our homes sales were from customer referrals!We couldn’t be prouder than to be an Eliant 2014 Homebuyers’ Choice Award Winner!"',
                'person' => 'Brian W. Doyle, President',
                'person_image' => 'brian-doyle.jpg',
                'company' => 'William Lyon Homes – California',
                'company_image' => 'william-lyon-homes.jpg',
            ],
            [
                'quote' => 'My divisions have been rated at or near the top of Eliant’s customer service ratings for over 20 years. No accident here; Eliant’s guidance and suggestions have helped us design a service experience that is second to none. We just follow their ‘Blueprint’.”',
                'person' => 'Dick Bryan, Director of Warranty Services',
                'person_image' => '',
                'company' => 'William Lyon Homes – Nevada',
                'company_image' => 'william-lyon-homes.jpg',
            ],
            [
                'quote' => 'The feedback we receive from your buyer surveys allows Classic Homes to keep our fingers on the pulse of our homeowners.We use the information and feedback from the survey reports to make changes in our products and processes.Your surveys and incredible daily reports have become more than a management tool; they are a part of the Classic culture."',
                'person' => 'Jerry Richardson, President',
                'person_image' => 'jerry-richardson.jpg',
                'company' => 'Classic Homes',
                'company_image' => 'classic-homes.jpg',
            ],
            [
                'quote' => 'Hands down, Eliant is the best annual investment we make in improving our customer referrals (which now deliver 32% of our sales!)."',
                'person' => 'Adrian Foley, President',
                'person_image' => 'adrian-foley.jpg',
                'company' => 'Brookfield Residential',
                'company_image' => 'brookfield-residential.jpg',
            ],

//            [
//                'quote' => '<p>Pro’s:</p>
//<ul>
//    <li>The integrated guides that highlight features.  Smart and well designed.</li>
//    <li>Graphs and trend lines.  Cleaner.</li>
//    <li>The option to choose between data that reflects COE or date survey received.</li>
//    <li>The red bar that reminds you to hit “refresh”</li>
//    <li>Reports are more intuitive and easier to read</li>
//    <li>Report cards that show Selected vs Prior period.  Reads well.  Like the individual ranking component as well.</li>
//    <li>Non respondent report – fantastic. </li>
//    <li>Trend lines for the various categories.  Spectacular.</li>
//    <li>Document section – superb across the board.</li>
//    <li>Contact page – extremely personable with your pictures and bios.</li>
//    <li>The fact that you’ll continue send the PDFs each month.  That was going to be a “game changer” in a bad way, if that service was discontinued.  Thanks for listening to my – and others’ – concerns.</li>
//</ul>',
//                'person' => 'Ralph Baja, National Director',
//                'company' => 'Benchmark communities- California'
//            ]
        ];
    }
}
