<?php

use Illuminate\Database\Seeder;

class WinnersListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lists = $this->getLists();

        foreach ($lists as $list) {
            \App\WinnersList::create($list);
        }
    }

    protected function winner($name, $location, $place = null, $tie = false)
    {
        return compact('name', 'location', 'place', 'tie');
    }

    protected function person($name, $companyName, $location, $place = null)
    {
        return compact('name', 'companyName', 'location', 'place');
    }

    protected function getLists()
    {
        return [
//            [
//                'name' => 'Top Builder Award Categories',
//                'description' => '40 Home builders receiving Premier Awards (First, Second, Third Place) or Honorable Mention awards include (in alphabetical order):',
//                'type' => 'simple',
//                'class' => 'builder',
//                'is_alphabetical' => 1,
//                'winners' => [
//                    $this->winner('Benchmark Communities', 'NorCal – Bay Area'),
//                    $this->winner('Benchmark Communities', 'NorCal – Central Valley'),
//                    $this->winner('Benchmark Communities', 'North Carolina'),
//                    $this->winner('Benchmark Communities', 'Southern California'),
//                    $this->winner('Brookfield Residential', 'Bay Area'),
//                    $this->winner('Brookfield Residential', 'Southern California'),
//                    $this->winner('Castle and Cooke Homes', 'Hawaii'),
//                    $this->winner('Gentry Homes', 'Hawaii'),
//                    $this->winner('Giddens Homes', 'Austin'),
//                    $this->winner('GL Homes of FL', 'Naples'),
//                    $this->winner('GL Homes of FL', 'Other Palm Beach'),
//                    $this->winner('GL Homes of FL', 'Palm Beach Adult'),
//                    $this->winner('GL Homes of FL', 'Palm Beach Family'),
//                    $this->winner('GL Homes of FL', 'Palm Beach Luxury'),
//                    $this->winner('Grand Homes', 'Dallas – East'),
//                    $this->winner('Irvine Pacific', 'Irvine, CA'),
//                    $this->winner('MBK Homes', 'Northern California'),
//                    $this->winner('MBK Homes', 'Southern California'),
//                    $this->winner('McKee Builders', 'Pennsylvania'),
//                    $this->winner('Minto Homes', 'Florida'),
//                    $this->winner('Qualico', 'Foxridge Homes, Vancouver, BC'),
//                    $this->winner('Qualico', 'StreetSide – Winnipeg'),
//                    $this->winner('Pardee Homes', 'Las Vegas – TRI Pointe Group'),
//                    $this->winner('Pardee Homes', 'San Diego – TRI Pointe Group'),
//                    $this->winner('Robson Resort Communities', 'Arizona'),
//                    $this->winner('Robson Resort Communities', 'Texas'),
//                    $this->winner('Rosewood Homes', 'Phoenix'),
//                    $this->winner('The McCaffrey Group', 'Fresno'),
//                    $this->winner('The New Home Company', 'Bay Area'),
//                    $this->winner('The New Home Company', 'Sacramento'),
//                    $this->winner('The New Home Company', 'Southern California'),
//                    $this->winner('The Olson Company', 'Seal Beach'),
//                    $this->winner('Tim Lewis Communities', 'Northern California'),
//                    $this->winner('TRI Pointe Group', 'Northern California – TRI Pointe Homes'),
//                    $this->winner('TRI Pointe Group', 'Southern California – TRI Pointe Homes'),
//                    $this->winner('Wathen Castanos', 'Fresno'),
//                    $this->winner('William Lyon Homes', 'Nevada'),
//                    $this->winner('William Lyon Homes', 'Southern California'),
//                    $this->winner('Woodside Homes', 'Fresno'),
//                ]
//            ],
            [
                'name' => 'Overall Home Purchase & Ownership Experience (“The Eliant”)',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Single Division Builders (regardless of sales volume)' => [
                        $this->winner('The Olsen Comany', 'Seal Beach, CA', 1),
                        $this->winner('Rosewood Homes', 'Phoenix', 2),
                        $this->winner('Wathen Castanos', 'Fresno', 3),
                        $this->winner('Giddens Homes', 'Austin', 'h'),
                        $this->winner('Minto Homes', 'Florida', 'h'),
                    ],
                    'Multi-Divisional Builders (regardless of sales volume)' => [
                        $this->winner('MBK Homes', 'Irvine, CA', 1),
                        $this->winner('The New Home Company', 'Aliso Viejo, CA', 2),
                        $this->winner('Robson Resord Communities', 'Robson Resort Communities - AZ/TX', 3),
                        $this->winner('TRI Pointe Group', 'Newport Beach, CA', 'h'),
                        $this->winner('Benchmark Communities', 'San Jose, CA', 'h'),
                    ]
                ]
            ],
            [
                'name' => 'Homebuyers’ Ratings of ‘The Purchase Experience’',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Medium Volume Builders (<125 closings in 2016)' => [
                        $this->winner('MBK Homes', 'Southern California', 1),
                        $this->winner('MBK Homes', 'Northern California', 2),
                        $this->winner('Rosewood Homes', 'Phoenix', 3),
                        $this->winner('Tim Lewis Communities', 'Northern California', 3),
                    ],
                    'Large Volume Builders (126-275 closings in 2016)' => [
                        $this->winner('The New Home Company', 'Southern California', 1),
                        $this->winner('Benchmark Communities', 'NorCal - Central Valley', 2),
                        $this->winner('The Olson Company', 'Southern California', 3),
                        $this->winner('Wathen Castanos', 'Fresno', 'h'),
                        $this->winner('Robson Resort Communities', 'Texas', 'h'),
                    ],
                    'High Volume Builders (276+ closings in 2016)' => [
                        $this->winner('Brookfield Residential', 'Southern California', 1),
                        $this->winner('William Lyon Homes', 'Southern California', 2),
                        $this->winner('Robson Resort Communities', 'Arizona', 3),
                        $this->winner('TRI Pointe Group', 'Southern California', 'h'),
                        $this->winner('TRI Pointe Group', 'Northern California', 'h'),
                    ],
                ]
            ],
            [
                'name' => 'The Design Selection Experience',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Medium Volume Builders (<125 closings in 2016)' => [
                        $this->winner('MBK Homes', 'Southern California', 1),
                        $this->winner('MBK Homes', 'Northern California', 2),
                        $this->winner('The New Home Company', 'Sacramento', 3),
                        $this->winner('Benchmark Communties', 'Southern California', 'h'),
                        $this->winner('McKee Builders', 'Pennsylvania', 'h'),
                    ],
                    'Large Volume Builders (126-275 closings in 2016)' => [
                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 1),
                        $this->winner('The Olson Company', 'Southern California', 2),
                        $this->winner('The New Home Company', 'Southern California', 3),
                        $this->winner('Wathen Castanos', 'Fresno', 'h'),
                        $this->winner('Robson Resort Communities', 'Texas', 'h'),
                    ],
                    'High Volume Builders (276+ closings in 2016)' => [
                        $this->winner('William Lyon Homes', 'Southern California', 1),
                        $this->winner('Minto Homes', 'Florida', 2),
                        $this->winner('Robson Resort Communities', 'Arizona', 3),
                        $this->winner('Pardee Homes', 'Las Vegas – TRI Pointe Group', 3),
                        $this->winner('TRI Pointe Homes', 'Southern California', 'h'),
                    ]
                ]
            ],
            [
                'name' => 'The Construction Experience',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Medium Volume Builders (<125 closings in 2016)' => [
                        $this->winner('MBK Homes', 'Southern California', 1),
                        $this->winner('MBK Homes', 'Northern California', 2),
                        $this->winner('Rosewood Homes', 'Phoenix', 3),
                        $this->winner('The New Home Company', 'Sacramento', 'h'),
                        $this->winner('Benchmark Communities', 'North Carolina', 'h'),
                    ],
                    'Large Volume Builders (126-275 closings in 2016)' => [
                        $this->winner('The New Home Company', 'Southern California', 1),
                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 2),
                        $this->winner('The Olson Company', 'Southern California', 3),
                        $this->winner('Wathen Castanos', 'Fresno', 'h'),
                        $this->winner('Robson Resort Communities', 'Texas', 'h'),
                        $this->winner('William Lyon Homes', 'Nevada', 'h')
                    ],
                    'High Volume Builders (276+ closings in 2016)' => [
                        $this->winner('William Lyon Homes', 'Southern California', 1),
                        $this->winner('TRI Pointe Group', 'Northern California – TRI Pointe Homes', 2),
                        $this->winner('TRI Pointe Group', 'Southern California – TRI Pointe Homes', 3),
                        $this->winner('Minto Homes', 'Florida', 'h'),
                        $this->winner('Brookfield Residential', 'Southern California', 'h'),
                    ],
                ]
            ],
            [
                'name' => 'The First-Year Customer Service Experience',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Medium Volume Builders (<125 closings in 2016)' => [
                        $this->winner('MBK Homes', 'Southern California', 1),
                        $this->winner('The New Home Company', 'Sacramento', 1),
                        $this->winner('Rosewood Homes', 'Phoenix', 3),
                        $this->winner('The New Home Company', 'Bay Area', 'h'),
                        $this->winner('Giddens Homes', 'Austin', 'h'),
                    ],
                    'Large Volume Builders (126-275 closings in 2016)' => [
                        $this->winner('The Olson Company', '', 1),
                        $this->winner('The New Home Company', 'Southern California', 2),
                        $this->winner('Grand Homes', 'Dallas – East', 3),
                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
                        $this->winner('Benchmark Communities', 'NorCal – Bay Area', 'h'),
                    ],
                    'High Volume Builders (276+ closings in 2016)' => [
                        $this->winner('TRI Pointe Homes', 'Northern California – TRI Pointe Group', 1),
                        $this->winner('William Lyon Homes', 'Southern California', 2),
                        $this->winner('Irvine Pacific', 'Irvine, CA', 3),
                        $this->winner('TRI Pointe Homes', 'Southern California – TRI Pointe Group', 'h'),
                        $this->winner('Robson Resort Communities', 'Arizona', 'h'),
                    ]
                ]
            ],
            [
                'name' => 'Overall First-Year Quality',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Medium Volume Builders (<125 closings in 2016)' => [
                        $this->winner('The New Home Company', 'Sacramento', 1),
                        $this->winner('Rosewood Homes', 'Phoenix', 2),
                        $this->winner('Giddens Homes', 'Austin', 3),
                        $this->winner('MBK Homes', 'Southern California', 'h'),
                        $this->winner('GL Homes of Florida', 'Other Palm Beach', 'h'),
                    ],
                    'Large Volume Builders (126-275 closings in 2016)' => [
                        $this->winner('The Olson Company', 'Southern California', 1),
                        $this->winner('The New Home Company', 'Southern California', 2),
                        $this->winner('Grand Homes', 'Dallas East', 3),
                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
                        $this->winner('Benchmark Communities', 'NorCal – Bay Area', 'h'),
                    ],
                    'High Volume Builders (276+ closings in 2016)' => [
                        $this->winner('Robson Resort Communities', 'Arizona', 1),
                        $this->winner('TRI Pointe Group', 'Northern California – TRI Pointe Homes', 2),
                        $this->winner('TRI Pointe Group', 'Southern California – TRI Pointe Homes', 3),
                        $this->winner('Minto Homes', 'Florida', 'h'),
                        $this->winner('Woodside Homes', 'Fresno', 'h'),
                    ]
                ]
            ],
            [
                'name' => 'Highest Percent of Sales from Referrals',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Medium Volume Builders' => [
                        $this->winner('Gentry Homes', 'Hawaii (50%)', 1),
                        $this->winner('MBK Homes', 'Southern California (42%)', 2),
                        $this->winner('Castle and Cooke Homes', 'Hawaii (37%)', 3),
                        $this->winner('McKee Builders', 'Pennsylvania (33%)', 'h'),
                        $this->winner('Qualico', 'StreetSide – Winnipeg (30%)', 'h'),
                    ],
                    'Large Volume Builders' => [
                        $this->winner('GL Homes of Florida', 'Palm Beach Adult (48%)', 1),
                        $this->winner('McCaffrey Group', 'Fresno (42%)', 2),
                        $this->winner('Qualico', 'Foxridge Homes – Vancouver (38%)', 3),
                        $this->winner('GL Homes of Florida', 'Palm Beach Luxury (33%)', 'h'),
                        $this->winner('GL Homes of Florida', 'Palm Beach Family (33%)', 'h'),
                    ],
                    'High Volume Builders' => [
                        $this->winner('Irvine Pacific', 'Irvine, CA (41%)', 1),
                        $this->winner('Brookfield Residential', 'Bay Area (35%)', 2),
                        $this->winner('GL Homes of Florida', 'Naples (34%)', 3),
                        $this->winner('TRI Pointe Homes', 'Northern California – TRI Pointe Group (34%)', 'h'),
                        $this->winner('Pardee Homes', 'San Diego – TRI Pointe Group (34%)', 'h'),
                    ]
                ]
            ],
            [
                'name' => 'Most Improved Builder',
                'description' => '',
                'type' => 'simple',
                'class' => 'builder',
                'winners' => [
                    $this->winner('GL Homes of Florida', 'Other Palm Beach', 1),
                    $this->winner('William Lyon', 'Southern California', 2),
                    $this->winner('Irvine Pacific', 'Southern California', 3),
                    $this->winner('GL Homes of Florida', 'Palm Beach Luxury', 'h'),
                    $this->winner('The New Home Company', 'Sacramento', 'h'),
                ]
            ],
            [
                'name' => 'Referral Driver Issues: Design Selection Experience: Perceived Value',
                'description' => '',
                'type' => 'simple',
                'class' => 'builder',
                'winners' => [
                    $this->winner('MBK Homes', 'Southern California', 1),
                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 2),
                    $this->winner('The New Home Company', 'Southern California', 3),
                    $this->winner('The Olson Company', 'Southern California', 'h'),
                    $this->winner('Pardee Homes', 'Las Vegas - TRI Pointe Group', 'h'),
                ]
            ],
            [
                'name' => 'Referral Driver Issues: Site Cleanliness',
                'description' => '',
                'type' => 'simple',
                'class' => 'builder',
                'winners' => [
                    $this->winner('MBK Homes', 'Southern California', 1),
                    $this->winner('William Lyon Homes', 'Southern California', 2),
                    $this->winner('The New Home Company', 'Southern California', 3),
                    $this->winner('The Olson Company', 'Southern California', 'h'),
                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
                ]
            ],
            [
                'name' => 'Referral Driver Issues: Pro-Active Communication of Construction Progress',
                'description' => '',
                'type' => 'simple',
                'class' => 'builder',
                'winners' => [
                    $this->winner('MBK Homes', 'Southern California', 1),
                    $this->winner('Brookfield Residential', 'Southern California', 2),
                    $this->winner('The Olson Company', 'Southern California', 3),
                    $this->winner('The New Home Company', 'Southern California', 'h'),
                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
                ]
            ],
            [
                'name' => 'Referral Driver Issues: Lender Proactive Communication of Loan Status',
                'description' => '',
                'type' => 'simple',
                'class' => 'builder',
                'winners' => [
                    $this->winner('MBK Homes', 'Southern California', 1),
                    $this->winner('William Lyon Homes', 'Nevada', 2),
                    $this->winner('Giddens Homes', 'Austin', 3),
                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
                    $this->winner('The New Home Company', 'Southern California', 'h'),
                ]
            ],
            [
                'name' => 'Referral Driver Issues: Warranty Service: Right the First Time',
                'description' => '',
                'type' => 'simple',
                'class' => 'builder',
                'winners' => [
                    $this->winner('MBK Homes', 'Southern California', 1),
                    $this->winner('The Olson Company', 'Southern California', 2),
                    $this->winner('Rosewood Homes', 'Phoenix', 3),
                    $this->winner('The New Home Company', 'Sacramento', 'h'),
                    $this->winner('The New Home Company', 'Southern California', 'h'),
                ]
            ],
            [
                'name' => 'Sales Representative of The Year',
                'description' => '',
                'type' => 'simple',
                'class' => 'individual',
                'winners' => [
                    $this->person('Julie Harwood', 'Brookfield Residential', 'Southern California', 1),
                    $this->person('Vallery DeVilbiss', 'Brookfield Residential', 'Southern California', 1),
                    $this->person('Matt Sbragia', 'TRI Pointe Homes', 'Northern California -TRI Pointe Group', 3),
                    $this->person('Heidi Chiang', 'MBK Homes', 'Southern California', 'h'),
                    $this->person('Sheril Alexandre', 'MBK Homes', 'Southern California', 'h'),
                ]
            ],
            [
                'name' => 'Construction Representative of The Year',
                'description' => '',
                'type' => 'simple',
                'class' => 'individual',
                'winners' => [
                    $this->person('Ryan Grexton', 'William Lyon Homes', 'Southern California', 1),
                    $this->person('Henry Van Curler', 'MBK Homes', 'Southern California', 2),
                    $this->person('John Fent', 'Benchmark Communities', 'NorCal – Central Valley', 3),
                    $this->person('Alan Howery', 'Benchmark Communities', 'NorCal – Central Valley', 'h'),
                    $this->person('Richard Hobar', 'Minto Homes', 'Florida', 'h'),
                ]
            ],
            [
                'name' => 'Design Representative of The Year',
                'description' => '',
                'type' => 'simple',
                'class' => 'individual',
                'winners' => [
                    $this->person('Heather Alexander', 'The Olson Company', 'Southern California', 1),
                    $this->person('Alyssa Matthews', 'MBK Homes', 'Southern California', 2),
                    $this->person('Andrea Polednak', 'Pardee Homes', 'Las Vegas – TRI Pointe Group', 2),
                    $this->person('Pamela Sadler', 'The New Home Company', 'Bay Area', 'h'),
                    $this->person('Donna Roveda', 'TRI Pointe Homes', 'Southern California – TRI Pointe Group', 'h'),
                ]
            ],
            [
                'name' => 'Customer Service Representative of The Year',
                'description' => '',
                'type' => 'simple',
                'class' => 'individual',
                'winners' => [
                    $this->person('Robert Lilly', 'MBK Homes', 'Southern California', 1),
                    $this->person('Eric Connolly & James Malcolm', 'The Olson Company', 'Southern California', 2),
                    $this->person('Todd Spofford', 'TRI Pointe Homes', 'Northern California – TRI Pointe Group', 2),
                    $this->person('Matt Smelser', 'The Olson Company', 'Southern California', 'h'),
                    $this->person('Bjarne Haukebo', 'The New Home Company', 'Southern California', 'h'),
                ]
            ],
            [
                'name' => 'Customer Experience Leader of the Year',
                'description' => '',
                'type' => 'simple',
                'class' => 'individual',
                'winners' => [
                    $this->person('Donna Velasco', 'MBK Homes', '', 1),
                ]
            ],
            [
                'name' => 'Overall Home Purchase & Ownership Experience (“The Eliant”)',
                'description' => '',
                'type' => 'categorized',
                'class' => 'builder',
                'winners' => [
                    'Single Division Builders (regardless of sales volume)' => [
                        $this->winner('The Olson Company', 'Seal Beach, CA', 1),
                        $this->winner('Rosewood Homes', 'Phoenix', 2),
                        $this->winner('Wathen Castanos', 'Fresno', 2),
                        $this->winner('Giddens Homes', 'Austin', 'h'),
                        $this->winner('Minto Homes', 'Florida', 'h'),
                    ],
                    'Multi-Divisional Builders (regardless of sales volume)' => [
                        $this->winner('MBK Homes', 'Irvine, CA', 1),
                        $this->winner('The New Home Company', 'Aliso Viejo, CA', 2),
                        $this->winner('Robson Resort Communities', 'AZ/TX', 3),
                        $this->winner('TRI Pointe Group', 'Newport Beach, CA', 'h'),
                        $this->winner('Benchmark Communities', 'San Jose, CA', 'h'),
                    ],
                ]
            ],
        ];
    }

//    Original getlist seeder below
//    protected function getLists()
//    {
//        return [
//            [
//                'name' => 'Top Builder Award Categories',
//                'description' => '40 Home builders receiving Premier Awards (First, Second, Third Place) or Honorable Mention awards include (in alphabetical order):',
//                'type' => 'simple',
//                'class' => 'builder',
//                'is_alphabetical' => 1,
//                'winners' => [
//                    $this->winner('Benchmark Communities', 'NorCal – Bay Area'),
//                    $this->winner('Benchmark Communities', 'NorCal – Central Valley'),
//                    $this->winner('Benchmark Communities', 'North Carolina'),
//                    $this->winner('Benchmark Communities', 'Southern California'),
//                    $this->winner('Brookfield Residential', 'Bay Area'),
//                    $this->winner('Brookfield Residential', 'Southern California'),
//                    $this->winner('Castle and Cooke Homes', 'Hawaii'),
//                    $this->winner('Gentry Homes', 'Hawaii'),
//                    $this->winner('Giddens Homes', 'Austin'),
//                    $this->winner('GL Homes of FL', 'Naples'),
//                    $this->winner('GL Homes of FL', 'Other Palm Beach'),
//                    $this->winner('GL Homes of FL', 'Palm Beach Adult'),
//                    $this->winner('GL Homes of FL', 'Palm Beach Family'),
//                    $this->winner('GL Homes of FL', 'Palm Beach Luxury'),
//                    $this->winner('Grand Homes', 'Dallas – East'),
//                    $this->winner('Irvine Pacific', 'Irvine, CA'),
//                    $this->winner('MBK Homes', 'Northern California'),
//                    $this->winner('MBK Homes', 'Southern California'),
//                    $this->winner('McKee Builders', 'Pennsylvania'),
//                    $this->winner('Minto Homes', 'Florida'),
//                    $this->winner('Qualico', 'Foxridge Homes, Vancouver, BC'),
//                    $this->winner('Qualico', 'StreetSide – Winnipeg'),
//                    $this->winner('Pardee Homes', 'Las Vegas – TRI Pointe Group'),
//                    $this->winner('Pardee Homes', 'San Diego – TRI Pointe Group'),
//                    $this->winner('Robson Resort Communities', 'Arizona'),
//                    $this->winner('Robson Resort Communities', 'Texas'),
//                    $this->winner('Rosewood Homes', 'Phoenix'),
//                    $this->winner('The McCaffrey Group', 'Fresno'),
//                    $this->winner('The New Home Company', 'Bay Area'),
//                    $this->winner('The New Home Company', 'Sacramento'),
//                    $this->winner('The New Home Company', 'Southern California'),
//                    $this->winner('The Olson Company', 'Seal Beach'),
//                    $this->winner('Tim Lewis Communities', 'Northern California'),
//                    $this->winner('TRI Pointe Group', 'Northern California – TRI Pointe Homes'),
//                    $this->winner('TRI Pointe Group', 'Southern California – TRI Pointe Homes'),
//                    $this->winner('Wathen Castanos', 'Fresno'),
//                    $this->winner('William Lyon Homes', 'Nevada'),
//                    $this->winner('William Lyon Homes', 'Southern California'),
//                    $this->winner('Woodside Homes', 'Fresno'),
//                ]
//            ],
//            [
//                'name' => 'Overall Home Purchase & Ownership Experience (“The Eliant”)',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Single Division Builders (regardless of sales volume)' => [
//                        $this->winner('The Olsen Comany', 'Seal Beach, CA', 1),
//                        $this->winner('Rosewood Homes', 'Phoenix', 2),
//                        $this->winner('Wathen Castanos', 'Fresno', 3),
//                        $this->winner('Giddens Homes', 'Austin', 'h'),
//                        $this->winner('Minto Homes', 'Florida', 'h'),
//                    ],
//                    'Multi-Divisional Builders (regardless of sales volume)' => [
//                        $this->winner('MBK Homes', 'Irvine, CA', 1),
//                        $this->winner('The New Home Company', 'Aliso Viejo, CA', 2),
//                        $this->winner('Robson Resord Communities', 'Robson Resort Communities - AZ/TX', 3),
//                        $this->winner('TRI Pointe Group', 'Newport Beach, CA', 'h'),
//                        $this->winner('Benchmark Communities', 'San Jose, CA', 'h'),
//                    ]
//                ]
//            ],
//            [
//                'name' => 'Homebuyers’ Ratings of ‘The Purchase Experience’',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Medium Volume Builders (<125 closings in 2016)' => [
//                        $this->winner('MBK Homes', 'Southern California', 1),
//                        $this->winner('MBK Homes', 'Northern California', 2),
//                        $this->winner('Rosewood Homes', 'Phoenix', 3),
//                        $this->winner('Tim Lewis Communities', 'Northern California', 3),
//                    ],
//                    'Large Volume Builders (126-275 closings in 2016)' => [
//                        $this->winner('The New Home Company', 'Southern California', 1),
//                        $this->winner('Benchmark Communities', 'NorCal - Central Valley', 2),
//                        $this->winner('The Olson Company', 'Southern California', 3),
//                        $this->winner('Wathen Castanos', 'Fresno', 'h'),
//                        $this->winner('Robson Resort Communities', 'Texas', 'h'),
//                    ],
//                    'High Volume Builders (276+ closings in 2016)' => [
//                        $this->winner('Brookfield Residential', 'Southern California', 1),
//                        $this->winner('William Lyon Homes', 'Southern California', 2),
//                        $this->winner('Robson Resort Communities', 'Arizona', 3),
//                        $this->winner('TRI Pointe Group', 'Southern California', 'h'),
//                        $this->winner('TRI Pointe Group', 'Northern California', 'h'),
//                    ],
//                ]
//            ],
//            [
//                'name' => 'The Design Selection Experience',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Medium Volume Builders (<125 closings in 2016)' => [
//                        $this->winner('MBK Homes', 'Southern California', 1),
//                        $this->winner('MBK Homes', 'Northern California', 2),
//                        $this->winner('The New Home Company', 'Sacramento', 3),
//                        $this->winner('Benchmark Communties', 'Southern California', 'h'),
//                        $this->winner('McKee Builders', 'Pennsylvania', 'h'),
//                    ],
//                    'Large Volume Builders (126-275 closings in 2016)' => [
//                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 1),
//                        $this->winner('The Olson Company', 'Southern California', 2),
//                        $this->winner('The New Home Company', 'Southern California', 3),
//                        $this->winner('Wathen Castanos', 'Fresno', 'h'),
//                        $this->winner('Robson Resort Communities', 'Texas', 'h'),
//                    ],
//                    'High Volume Builders (276+ closings in 2016)' => [
//                        $this->winner('William Lyon Homes', 'Southern California', 1),
//                        $this->winner('Minto Homes', 'Florida', 2),
//                        $this->winner('Robson Resort Communities', 'Arizona', 3),
//                        $this->winner('Pardee Homes', 'Las Vegas – TRI Pointe Group', 3),
//                        $this->winner('TRI Pointe Homes', 'Southern California', 'h'),
//                    ]
//                ]
//            ],
//            [
//                'name' => 'The Construction Experience',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Medium Volume Builders (<125 closings in 2016)' => [
//                        $this->winner('MBK Homes', 'Southern California', 1),
//                        $this->winner('MBK Homes', 'Northern California', 2),
//                        $this->winner('Rosewood Homes', 'Phoenix', 3),
//                        $this->winner('The New Home Company', 'Sacramento', 'h'),
//                        $this->winner('Benchmark Communities', 'North Carolina', 'h'),
//                    ],
//                    'Large Volume Builders (126-275 closings in 2016)' => [
//                        $this->winner('The New Home Company', 'Southern California', 1),
//                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 2),
//                        $this->winner('The Olson Company', 'Southern California', 3),
//                        $this->winner('Wathen Castanos', 'Fresno', 'h'),
//                        $this->winner('Robson Resort Communities', 'Texas', 'h'),
//                        $this->winner('William Lyon Homes', 'Nevada', 'h')
//                    ],
//                    'High Volume Builders (276+ closings in 2016)' => [
//                        $this->winner('William Lyon Homes', 'Southern California', 1),
//                        $this->winner('TRI Pointe Group', 'Northern California – TRI Pointe Homes', 2),
//                        $this->winner('TRI Pointe Group', 'Southern California – TRI Pointe Homes', 3),
//                        $this->winner('Minto Homes', 'Florida', 'h'),
//                        $this->winner('Brookfield Residential', 'Southern California', 'h'),
//                    ],
//                ]
//            ],
//            [
//                'name' => 'The First-Year Customer Service Experience',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Medium Volume Builders (<125 closings in 2016)' => [
//                        $this->winner('MBK Homes', 'Southern California', 1),
//                        $this->winner('The New Home Company', 'Sacramento', 1),
//                        $this->winner('Rosewood Homes', 'Phoenix', 3),
//                        $this->winner('The New Home Company', 'Bay Area', 'h'),
//                        $this->winner('Giddens Homes', 'Austin', 'h'),
//                    ],
//                    'Large Volume Builders (126-275 closings in 2016)' => [
//                        $this->winner('The Olson Company', '', 1),
//                        $this->winner('The New Home Company', 'Southern California', 2),
//                        $this->winner('Grand Homes', 'Dallas – East', 3),
//                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
//                        $this->winner('Benchmark Communities', 'NorCal – Bay Area', 'h'),
//                    ],
//                    'High Volume Builders (276+ closings in 2016)' => [
//                        $this->winner('TRI Pointe Homes', 'Northern California – TRI Pointe Group', 1),
//                        $this->winner('William Lyon Homes', 'Southern California', 2),
//                        $this->winner('Irvine Pacific', 'Irvine, CA', 3),
//                        $this->winner('TRI Pointe Homes', 'Southern California – TRI Pointe Group', 'h'),
//                        $this->winner('Robson Resort Communities', 'Arizona', 'h'),
//                    ]
//                ]
//            ],
//            [
//                'name' => 'Overall First-Year Quality',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Medium Volume Builders (<125 closings in 2016)' => [
//                        $this->winner('The New Home Company', 'Sacramento', 1),
//                        $this->winner('Rosewood Homes', 'Phoenix', 2),
//                        $this->winner('Giddens Homes', 'Austin', 3),
//                        $this->winner('MBK Homes', 'Southern California', 'h'),
//                        $this->winner('GL Homes of Florida', 'Other Palm Beach', 'h'),
//                    ],
//                    'Large Volume Builders (126-275 closings in 2016)' => [
//                        $this->winner('The Olson Company', 'Southern California', 1),
//                        $this->winner('The New Home Company', 'Southern California', 2),
//                        $this->winner('Grand Homes', 'Dallas East', 3),
//                        $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
//                        $this->winner('Benchmark Communities', 'NorCal – Bay Area', 'h'),
//                    ],
//                    'High Volume Builders (276+ closings in 2016)' => [
//                        $this->winner('Robson Resort Communities', 'Arizona', 1),
//                        $this->winner('TRI Pointe Group', 'Northern California – TRI Pointe Homes', 2),
//                        $this->winner('TRI Pointe Group', 'Southern California – TRI Pointe Homes', 3),
//                        $this->winner('Minto Homes', 'Florida', 'h'),
//                        $this->winner('Woodside Homes', 'Fresno', 'h'),
//                    ]
//                ]
//            ],
//            [
//                'name' => 'Highest Percent of Sales from Referrals',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Medium Volume Builders' => [
//                        $this->winner('Gentry Homes', 'Hawaii (50%)', 1),
//                        $this->winner('MBK Homes', 'Southern California (42%)', 2),
//                        $this->winner('Castle and Cooke Homes', 'Hawaii (37%)', 3),
//                        $this->winner('McKee Builders', 'Pennsylvania (33%)', 'h'),
//                        $this->winner('Qualico', 'StreetSide – Winnipeg (30%)', 'h'),
//                    ],
//                    'Large Volume Builders' => [
//                        $this->winner('GL Homes of Florida', 'Palm Beach Adult (48%)', 1),
//                        $this->winner('McCaffrey Group', 'Fresno (42%)', 2),
//                        $this->winner('Qualico', 'Foxridge Homes – Vancouver (38%)', 3),
//                        $this->winner('GL Homes of Florida', 'Palm Beach Luxury (33%)', 'h'),
//                        $this->winner('GL Homes of Florida', 'Palm Beach Family (33%)', 'h'),
//                    ],
//                    'High Volume Builders' => [
//                        $this->winner('Irvine Pacific', 'Irvine, CA (41%)', 1),
//                        $this->winner('Brookfield Residential', 'Bay Area (35%)', 2),
//                        $this->winner('GL Homes of Florida', 'Naples (34%)', 3),
//                        $this->winner('TRI Pointe Homes', 'Northern California – TRI Pointe Group (34%)', 'h'),
//                        $this->winner('Pardee Homes', 'San Diego – TRI Pointe Group (34%)', 'h'),
//                    ]
//                ]
//            ],
//            [
//                'name' => 'Most Improved Builder',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'builder',
//                'winners' => [
//                    $this->winner('GL Homes of Florida', 'Other Palm Beach', 1),
//                    $this->winner('William Lyon', 'Southern California', 2),
//                    $this->winner('Irvine Pacific', 'Southern California', 3),
//                    $this->winner('GL Homes of Florida', 'Palm Beach Luxury', 'h'),
//                    $this->winner('The New Home Company', 'Sacramento', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Referral Driver Issues: Design Selection Experience: Perceived Value',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'builder',
//                'winners' => [
//                    $this->winner('MBK Homes', 'Southern California', 1),
//                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 2),
//                    $this->winner('The New Home Company', 'Southern California', 3),
//                    $this->winner('The Olson Company', 'Southern California', 'h'),
//                    $this->winner('Pardee Homes', 'Las Vegas - TRI Pointe Group', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Referral Driver Issues: Site Cleanliness',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'builder',
//                'winners' => [
//                    $this->winner('MBK Homes', 'Southern California', 1),
//                    $this->winner('William Lyon Homes', 'Southern California', 2),
//                    $this->winner('The New Home Company', 'Southern California', 3),
//                    $this->winner('The Olson Company', 'Southern California', 'h'),
//                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Referral Driver Issues: Pro-Active Communication of Construction Progress',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'builder',
//                'winners' => [
//                    $this->winner('MBK Homes', 'Southern California', 1),
//                    $this->winner('Brookfield Residential', 'Southern California', 2),
//                    $this->winner('The Olson Company', 'Southern California', 3),
//                    $this->winner('The New Home Company', 'Southern California', 'h'),
//                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Referral Driver Issues: Lender Proactive Communication of Loan Status',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'builder',
//                'winners' => [
//                    $this->winner('MBK Homes', 'Southern California', 1),
//                    $this->winner('William Lyon Homes', 'Nevada', 2),
//                    $this->winner('Giddens Homes', 'Austin', 3),
//                    $this->winner('Benchmark Communities', 'NorCal – Central Valley', 'h'),
//                    $this->winner('The New Home Company', 'Southern California', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Referral Driver Issues: Warranty Service: Right the First Time',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'builder',
//                'winners' => [
//                    $this->winner('MBK Homes', 'Southern California', 1),
//                    $this->winner('The Olson Company', 'Southern California', 2),
//                    $this->winner('Rosewood Homes', 'Phoenix', 3),
//                    $this->winner('The New Home Company', 'Sacramento', 'h'),
//                    $this->winner('The New Home Company', 'Southern California', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Sales Representative of The Year',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'individual',
//                'winners' => [
//                    $this->person('Julie Harwood', 'Brookfield Residential', 'Southern California', 1),
//                    $this->person('Vallery DeVilbiss', 'Brookfield Residential', 'Southern California', 1),
//                    $this->person('Matt Sbragia', 'TRI Pointe Homes', 'Northern California -TRI Pointe Group', 3),
//                    $this->person('Heidi Chiang', 'MBK Homes', 'Southern California', 'h'),
//                    $this->person('Sheril Alexandre', 'MBK Homes', 'Southern California', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Construction Representative of The Year',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'individual',
//                'winners' => [
//                    $this->person('Ryan Grexton', 'William Lyon Homes', 'Southern California', 1),
//                    $this->person('Henry Van Curler', 'MBK Homes', 'Southern California', 2),
//                    $this->person('John Fent', 'Benchmark Communities', 'NorCal – Central Valley', 3),
//                    $this->person('Alan Howery', 'Benchmark Communities', 'NorCal – Central Valley', 'h'),
//                    $this->person('Richard Hobar', 'Minto Homes', 'Florida', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Design Representative of The Year',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'individual',
//                'winners' => [
//                    $this->person('Heather Alexander', 'The Olson Company', 'Southern California', 1),
//                    $this->person('Alyssa Matthews', 'MBK Homes', 'Southern California', 2),
//                    $this->person('Andrea Polednak', 'Pardee Homes', 'Las Vegas – TRI Pointe Group', 2),
//                    $this->person('Pamela Sadler', 'The New Home Company', 'Bay Area', 'h'),
//                    $this->person('Donna Roveda', 'TRI Pointe Homes', 'Southern California – TRI Pointe Group', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Customer Service Representative of The Year',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'individual',
//                'winners' => [
//                    $this->person('Robert Lilly', 'MBK Homes', 'Southern California', 1),
//                    $this->person('Eric Connolly & James Malcolm', 'The Olson Company', 'Southern California', 2),
//                    $this->person('Todd Spofford', 'TRI Pointe Homes', 'Northern California – TRI Pointe Group', 2),
//                    $this->person('Matt Smelser', 'The Olson Company', 'Southern California', 'h'),
//                    $this->person('Bjarne Haukebo', 'The New Home Company', 'Southern California', 'h'),
//                ]
//            ],
//            [
//                'name' => 'Customer Experience Leader of the Year',
//                'description' => '',
//                'type' => 'simple',
//                'class' => 'individual',
//                'winners' => [
//                    $this->person('Donna Velasco', 'MBK Homes', '', 1),
//                ]
//            ],
//            [
//                'name' => 'Overall Home Purchase & Ownership Experience (“The Eliant”)',
//                'description' => '',
//                'type' => 'categorized',
//                'class' => 'builder',
//                'winners' => [
//                    'Single Division Builders (regardless of sales volume)' => [
//                        $this->winner('The Olson Company', 'Seal Beach, CA', 1),
//                        $this->winner('Rosewood Homes', 'Phoenix', 2),
//                        $this->winner('Wathen Castanos', 'Fresno', 2),
//                        $this->winner('Giddens Homes', 'Austin', 'h'),
//                        $this->winner('Minto Homes', 'Florida', 'h'),
//                    ],
//                    'Multi-Divisional Builders (regardless of sales volume)' => [
//                        $this->winner('MBK Homes', 'Irvine, CA', 1),
//                        $this->winner('The New Home Company', 'Aliso Viejo, CA', 2),
//                        $this->winner('Robson Resort Communities', 'AZ/TX', 3),
//                        $this->winner('TRI Pointe Group', 'Newport Beach, CA', 'h'),
//                        $this->winner('Benchmark Communities', 'San Jose, CA', 'h'),
//                    ],
//                ]
//            ],
//        ];
//    }
}
