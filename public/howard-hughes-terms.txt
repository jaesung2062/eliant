NOTE: Howard Hughes will be awarding two Fandango gift cards for each completed survey response. In addition, each completed survey response is eligible to win a $1000 Whole Foods gift card. Each drawing is randomly selected and completed surveys must be submitted by the due date on the email invitation.

Eliant, Inc. does not sell, exchange, or distribute your personal information. Please see our privacy statement at http://www.eliant.com/privacy for more information. To unsubscribe from any future emails from Eliant, please visit http://cem.eliant.com/optout.aspx.

Disclaimer: Howard Hughes and Eliant, Inc. is not affiliated with Fandango or Whole Foods.