/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(function () {
    require('./modules/animations');
    require('./modules/navbar');
    require('./modules/common');
    require('./modules/admin');

    if (window.faqs) {
        require('./modules/faq');
    }
});

