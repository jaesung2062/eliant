window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
window.axios = require('axios');
window.flickity = require('flickity');

require('bootstrap-sass');
require('jquery-match-height/dist/jquery.matchHeight-min.js');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};
