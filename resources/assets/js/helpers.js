export function wait(ms) {
    return new Promise(function(resolve) {
        setTimeout(function() {
            resolve();

            return wait;
        }, ms);
    });
}


// export function wait(ms) {
//     return new Promise((resolve) => {
//         setTimeout(() => {
//             resolve();
//
//             return wait;
//         }, ms);
//     });
// }
