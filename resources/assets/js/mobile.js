require('./bootstrap');

import $ from 'jquery';

$(function () {
    (function navbar() {
		let isOpening,
			isClosing;

		resizeNav(2);

		$(window).resize(function () {
			resizeNav(2);
		});

        $(window).scroll(function () {
                const scrollPosition = $(window).scrollTop();

                if (scrollPosition > 0) {
                    resizeNav(4);
                }
                else {
                    resizeNav(2);
                }
        });

        // open menu
        $('.menu-dropdown-button').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            $('.rate-trades-login-dropdown, .eliant-portal-login-dropdown').slideUp('1s ease-in-out');

            openMenu();
        });

        $('.menu-close-button').click(function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            closeMenu();
        });

		function resizeNav(divisor) {
			// if screen width > 800, always make navbar half-height regardless
			if (window.innerWidth > 800) {
				divisor = 4;
			}

			const height = $(window).width() / divisor;

			$('#navbar').height(height);

			$('body').css({
				paddingTop: height + 'px'
			});
		}

        function openMenu() {
            if (isOpening) return;
            isOpening = true;

            $('#menu').show();

            setTimeout(() => {
                $('#menu').addClass('open');
            });

            setTimeout(() => {
                isOpening = false;
            }, 1300);
        }

        function closeMenu() {
            if (isClosing) return;
            isClosing = false;

            $('#menu').removeClass('open');

            setTimeout(() => {
                $('#menu').hide();
                isClosing = false;
            }, 1300);
        }
    })();

    (function faq() {
		const Fuse = require('fuse.js/dist/fuse.js');
		const $answersContainer = $('.answers-container');

		window.faqs.forEach((faq, index) => {
			faq.id = index + 1;

			$answersContainer.append(`
<div class="question-answer" data-faq-id="${faq.id}">
    <a name="question_${index}" class="anchor"></a>

    <h3 class="question">${faq.question}</h3>

    <p class="answer">${faq.answer}</p>
</div>`);
		});

		let options = {
			shouldSort: true,
			threshold: 0.1,
			location: 0,
			distance: 0,
			maxPatternLength: 32,
			minMatchCharLength: 1,
			keys: [
				'question',
				'answer'
			]
		};
		const fuse = new Fuse(faqs, options);

		$('#faq-search-input').keyup(function (e) {
			const $input = $(this);

			if ($input.val().length === 0) {
				$('[data-faq-id]').show();
				$('.none-found-message').hide();
			}
			else {
				const results = fuse.search($input.val());

				if (results.length === 0) {
					$('.none-found-message').show();
				}
				else {
					$('.none-found-message').hide();
				}

				$('[data-faq-id]').hide();

				results.forEach((faq) => {
					$(`[data-faq-id="${faq.id}"]`).show();
				});
			}

			// scroll to questions
			if (e.keyCode === 13) {
				$('body').animate({scrollTop: $('.section-search').position().top}, 300);
			}
		});
	})();
});
