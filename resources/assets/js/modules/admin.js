const Sortable = require('sortablejs/Sortable.min.js');

$('.dashboard-nav-link').matchHeight();

$('[data-delete]').click(function () {
    const $this = $(this);
    const id = $this.data('delete');
    const resource = $this.data('resource');

    axios.delete('/admin/' + resource + '/' + id).then(function () {
        window.location.reload();
    });
});

$('.sortable').each(function () {
    const $sortable = $(this);

    Sortable.create(this, {
        onSort: () => {
            if ($sortable.data('postOnReorder') === true) {
                const order = [];

                $sortable.children().each(function () {
                    order.push($(this).data('modelId'));
                });

                axios.post('/admin/testimonials/reorder', {order});
            }
        },
    });
});

$('.btn-add-winner-simple').click(function () {
    const $clone = $(this).closest('.form-simple').find('.winner-row').first().clone();
    const index = $(this).closest('.form-simple').find('.winner-row').length;

    $clone.find('input').val('');
    $clone.find('.winner-input-name').attr('name', `winners[${index}][name]`);
    $clone.find('.winner-input-location').attr('name', `winners[${index}][location]`);

    $('.simple-winners-container').append($clone);
});

(function categorized() {
    let $categoriesContainer = $('.categories-container');

    $('.form-categorized').submit(function (e) {
        const $this = $(this);

        if (! canSubmit($this)) {
            e.preventDefault();
            alert('There are errors in your input. The category name must be filled, and cannot be the same as other category fields.');
        }

        function canSubmit() {
            let canSubmitForm = true;
            let categories = [];

            $this.find('.category-input').each(function () {
                let category = $(this).val();

                if (categories.indexOf(category) > -1 || category === '') {
                    canSubmitForm = false;
                }

                categories.push(category);
            });

            return canSubmitForm;
        }
    });

    $categoriesContainer.on('change', '.category-input', function () {
        const $this = $(this);
        const $categoryContainer = $this.closest('.category-container');
        const category = $this.val();
        const $winnerRow = $categoryContainer.find('.winner-row');

        $winnerRow.each(function () {
            const $this = $(this);
            const index = $this.data('index');

            $this.find('.winner-input-name').attr('name', `winners[${category}][${index}][name]`);
            $this.find('.winner-input-location').attr('name', `winners[${category}][${index}][location]`);
        });
    });

    $categoriesContainer.on('click', '.btn-add-winner-categorized', function () {
        const $categoryContainer = $(this).closest('.category-container');
        const $clone = $categoryContainer.find('.winner-row').first().clone();
        const category = $categoryContainer.val();
        const index = $categoryContainer.find('.winner-row').length;

        $clone.attr('data-index', index).find('input').val('');
        $clone.find('option').prop('checked', false);
        $clone.find('select').attr('name', `winners[${category}][${index}][place]`);
        $clone.find('.winner-input-name').attr('name', `winners[${category}][${index}][name]`);
        $clone.find('.winner-input-location').attr('name', `winners[${category}][${index}][location]`);

        $categoryContainer.find('.category-winners-container').append($clone);
    });

    $('.btn-add-category').click(function () {
        const $clone = $('.category-container').first().clone();

        $clone.find('input').val('');
        $clone.find('.winner-row:not(:first-child)').remove();

        $('.categories-container').append($clone);
    });
})();
