// import { wait } from '../helpers';

var $first = $('.header-images-container img:first-child');
var $last = $('.header-images-container img:last-child');

// wait(500).then(() => {
//     $first.addClass('activated');
//
//     wait(600).then(() => {
//         $last.addClass('activated');
//     });
// });

// wait(500).then(function () {
//     $first.addClass('activated');
//
//     wait(600).then(function () {
//         $last.addClass('activated');
//     });
// });



$(document).ready(function () {
    $('.animate').addClass('activate');



    setTimeout(function () {
        $first.addClass('activated');
    }, 500);

    setTimeout(function () {
        $last.addClass('activated');
    }, 1100);
});


var $subsections = $('.contains-animations');

activateAnimations();

$(window).scroll(function () {
    activateAnimations();
});

function activateAnimations() {
    $subsections.each(function () {
        let $this = $(this);

        if (isElementInViewport($this)) {
            $this.find('.grid-image, .text-container, .copy-container, .content-container, .image-column-1, .image-column-2, .fade-in-up').addClass('activated');
        }
    });

    $('.client-logo-container').each(function () {
        if (isElementInViewport($(this))) {
            $(this).addClass('activated');
        }
    });
}

function isElementInViewport(el) {
    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    let rect = el.getBoundingClientRect();

    return (
        rect.bottom >= 0 &&
        rect.right >= 0 &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
    );
}
