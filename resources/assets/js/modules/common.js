import { wait } from '../helpers';

$('.volume-icon-container-outer').matchHeight();
$('.volume-icon-container').matchHeight();
$('.row.testimonial > *').matchHeight({byRow: true});
$('.row.blog-listing > *').matchHeight({byRow: true});

// contact form
var $formInputs = $('.contact-form .form-control');



$formInputs.focus(function () {
    $(this).siblings().fadeOut('fast');
});

$formInputs.blur(function () {
    if ($(this).val().length === 0) {
        $(this).siblings().fadeIn('fast');
    }
});

$('.link-sponsor-modal').click(function (e) {
    e.preventDefault();

    var type = $(this).data('type');

    $('#sponsor-modal').modal('toggle').find(`option[value="${type}"]`).attr('selected', 'selected');
});

$('#sponsor-form').submit(function (e) {
    e.preventDefault();

    var $errorsContainer = $('#sponsor-form-errors');
    let dataArray = $(this).serializeArray();
    let dataObject = {};

    dataArray.forEach((input) => {
        dataObject[input.name] = input.value;
    });

    axios.post('/sponsors', dataObject)
        .then(() => {
            $errorsContainer.addClass('hidden').find('ul').empty();
            $('#sponsor-modal').modal('toggle');
            $('html,body').scrollTop();
            $('.page').prepend($('<div class="alert alert-success">You have successfully sent the message!</div>'));
        })
        .catch((error) => {
            var errors = error.response.data;

            for (let key in errors) {
                if (errors.hasOwnProperty(key)) {
                    $errorsContainer.find('ul').append(`<li>${errors[key]}</li>`)
                }
            }

            $errorsContainer.removeClass('hidden');
        });
});

$('.share-links a').click(function (e) {
    e.preventDefault();
	// window.open(
	// 	'http://www.facebook.com/share.php?u=http://example.com&title=my+title',
	// 	'_blank',
	// 	'location=yes,height=570,width=520,scrollbars=yes,status=yes'
	// );
});

//Detect version
// Get IE or Edge browser version
var version = detectIE();

if (version === false) {
    document.getElementById('result').innerHTML = '<s>IE/Edge</s>';
} else if (version >= 12) {
    document.getElementById('result').innerHTML = 'Edge ' + version;
} else {
    document.getElementById('result').innerHTML = 'IE ' + version;
}

// add details to debug result
//document.getElementById('details').innerHTML = window.navigator.userAgent;

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}
