var Fuse = require('fuse.js/dist/fuse.js');


var $questionsContainer = $('.questions-container');
var $answersContainer = $('.answers-container');

window.faqs.forEach((faq, index) => {
    faq.id = index + 1;

    // todo add to fuse index

    $questionsContainer.append(`
<div class="question-link text-uppercase" data-faq-id="${faq.id}">
    <a href="#question_${index}">${faq.question}</a>
</div>`);

    $answersContainer.append(`
<div class="question-answer" data-faq-id="${faq.id}">
    <a name="question_${index}" class="anchor"></a>

    <h3 class="question">${faq.question}</h3>

    <p class="answer">${faq.answer}</p>
</div>`);
});

let options = {
    shouldSort: true,
    threshold: 0.5,
    distance: 1000,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
        'question',
        'answer'
    ]
};
var fuse = new Fuse(faqs, options);

$('#faq-search-input').keyup(function (e) {
    var $input = $(this);

    if ($input.val().length === 0) {
        $('[data-faq-id]').show();
        $('.none-found-message').hide();
    }
    else {
        var results = fuse.search($input.val());

        if (results.length === 0) {
            $('.none-found-message').show();
        }
        else {
            $('.none-found-message').hide();
        }

        $('[data-faq-id]').hide();

        results.forEach((faq) => {
            $(`[data-faq-id="${faq.id}"]`).show();
        });
    }

    // scroll to questions
    if (e.keyCode === 13) {
        $('body').animate({scrollTop: $('.section-search').position().top}, 300);
    }
});