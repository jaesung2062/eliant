// import { wait } from '../helpers';

var isOpening = false;
var isClosing = false;

//open menu
$('.menu-dropdown-button').click(function (e) {
    e.preventDefault();
    e.stopPropagation();

    $('.rate-trades-login-dropdown, .eliant-portal-login-dropdown').slideUp('1s ease-in-out');

    openMenu();
});

$('.menu-close-button').click(function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    closeMenu();
});

// $('.rate-trades-button').click(function (e) {
//     e.preventDefault();
//     e.stopPropagation();
//
//     $('.eliant-portal-login-dropdown').slideUp('1s ease-in-out');
//     $('.rate-trades-login-dropdown').slideToggle('1s ease-in-out');
//     $('.nav-brand-container').addClass('closed');
// });
//
// $('.eliant-portal-button').click(function (e) {
//     e.preventDefault();
//     e.stopPropagation();
//
//     $('.rate-trades-login-dropdown').slideUp('1s ease-in-out');
//     $('.eliant-portal-login-dropdown').slideToggle('1s ease-in-out');
//     $('.nav-brand-container').addClass('closed');
// });

$('#menu a:not(.menu-close-button)').click(function (e) {
    e.preventDefault();

    closeMenu();

    setTimeout(function () {
        var $target;

        if ($(e.target).is('a')) {
            $target = $(e.target);
        }
        else {
            $target = $(e.target).closest('a');
        }

        window.location.href = $target.attr('href');
    }, 1300);
});

$('body').click(function (e) {
    $('#menu, .rate-trades-login-dropdown, .eliant-portal-login-dropdown').slideUp('fast');

    if ($(window).scrollTop() === 0) {
        $('.nav-brand-container').removeClass('closed');
    }
});

$(window).scroll(function () {
    if ($(window).scrollTop() === 0) {
        $('.nav-brand-container').removeClass('closed');
    }
    else {
        $('.nav-brand-container').addClass('closed');
    }
});

$('html, body').scroll(function () {
    if ($('html, body').scrollTop() === 0) {
        $('.nav-brand-container').removeClass('closed');
    }
    else {
        $('.nav-brand-container').addClass('closed');
    }
});

$('.login-dropdown, #menu').click(function (e) {
    e.stopPropagation();
});

$('.menu-link').hover(function () {
    $('.menu-link-preview').text($(this).text());
});

$(document).keyup(function (e) {
    if (e.keyCode === 27) {
        closeMenu();
    }
});

function openMenu() {
    if (isOpening) return;
    isOpening = true;

    $('#menu').show();

    setTimeout(function() {
        $('#menu').addClass('open');
    });

    // setTimeout(() => {
    //     $('#menu').addClass('open');
    // });

    setTimeout(function() {
        isOpening = false;
    }, 1300);

    // setTimeout(() => {
    //     isOpening = false;
    // }, 1300);
}

function closeMenu() {
    if (isClosing) return;
    isClosing = false;

    $('#menu').removeClass('open');

    setTimeout(() => {
        $('#menu').hide();
        isClosing = false;
    }, 1300);
}
