@extends('default')

@section('content')
    <div class="page admin-page page-dashboard">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>Welcome to the Eliant Dashboard.</h1>

                    <p>
                        You can edit certain pages within the site structure. You cannot add any pages at this time,
                        however if you so desire, please let us know. For any technical support or help with the Content
                        Management System, please contact michael@wbrandstudio.com
                    </p>

                    <a href="/admin/testimonials" class="btn btn-primary btn-lg dashboard-nav-link"><span>Edit Testimonials</span></a>
                    <a href="/admin/testimonials" class="btn btn-primary btn-lg dashboard-nav-link"><span>Edit Blog</span></a>
                    <a href="/admin/awards" class="btn btn-primary btn-lg dashboard-nav-link"><span>Edit Home Buyer's Choice Awards</span></a>
                </div>
            </div>
        </div>
    </div>
@endsection
