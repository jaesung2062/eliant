@extends('default')

@section('content')
    <div class="page admin-page page-admin-testimonials-create">
        <div class="container">
            @include('partials.admin-nav')
            @include('partials.alerts')

            <h1 class="text-center">Edit Testimonial</h1>

            <hr>

            {{--<form action="upload.php" method="post" enctype="multipart/form-data">--}}
                {{--Select image to upload:--}}
                {{--<input type="file" name="fileToUpload" id="fileToUpload">--}}
                {{--<input type="submit" value="Upload Image" name="submit">--}}
            {{--</form>--}}


            <form action="/admin/testimonials/" method="post">
                {{ csrf_field() }}

                <label>Image</label>
                <input type="file" name="avatar">


                <div class="form-group row">
                    <label for="person" class="col-xs-3 col-form-label text-right">Person</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="person" name="person" placeholder="Person" value="{{ old('person') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="company" class="col-xs-3 col-form-label text-right">Company</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="company" name="company" placeholder="Company" value="{{ old('company') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="quote" class="col-xs-3 col-form-label text-right">Quote</label>
                    <div class="col-xs-9">
                        <textarea name="quote" id="quote" cols="30" rows="10" class="form-control" placeholder="Quote">{{ old('quote') }}</textarea>
                    </div>
                </div>


                <div class="text-right row">
                    <div class="col-xs-9 col-xs-offset-3">
                        <hr>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
