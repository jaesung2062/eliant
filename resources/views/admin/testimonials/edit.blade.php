@extends('default')

@section('content')
    <div class="page admin-page page-admin-testimonials-edit">
        <div class="container">
            @include('partials.admin-nav')
            @include('partials.alerts')

            <h1 class="text-center">Edit Testimonial</h1>

            <hr>

            <form action="/admin/testimonials/{{ $testimonial->id }}" method="post">
                {{ csrf_field() }}
                {{ method_field('patch') }}

                <div class="form-group row">
                    <label for="person" class="col-xs-3 col-form-label text-right">Person</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="person" name="person" placeholder="Person" value="{{ old('person') ?: $testimonial->person }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="company" class="col-xs-3 col-form-label text-right">Company</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="company" name="company" placeholder="Company" value="{{ old('company') ?: $testimonial->company }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="quote" class="col-xs-3 col-form-label text-right">Quote</label>
                    <div class="col-xs-9">
                        <textarea name="quote" id="quote" cols="30" rows="10" class="form-control" placeholder="Quote">{{ old('quote') ?: $testimonial->quote }}</textarea>
                    </div>
                </div>

                <div class="text-right row">
                    <div class="col-xs-9 col-xs-offset-3">
                        <hr>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
