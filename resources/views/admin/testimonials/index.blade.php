@extends('default')

@section('content')
    <div class="page admin-page page-admin-testimonials-index">
        <div class="container">
            @include('partials.admin-nav')
            @include('partials.alerts')

            <h1 class="text-center">Testimonials</h1>

            <div class="text-center">
                <a href="/admin/testimonials/create" class="btn btn-primary">Create</a>
                <br>
                <br>
            </div>

            <div class="sortable sortable-list" data-resource="testimonials" data-post-on-reorder="true">
                @foreach($testimonials as $testimonial)
                    <div class="sortable-entry row" data-model-id="{{ $testimonial->id }}">
                        <a href="/admin/testimonials/{{ $testimonial->id }}/edit" class="col-xs-10 testimonial-link">{{ $testimonial->person }}</a>
                        <a href="#" class="col-xs-1 action-button testimonial-delete-button" data-delete="{{ $testimonial->id }}" data-resource="testimonials"><i class="fa fa-trash"></i></a>
                        <a href="/admin/testimonials/{{ $testimonial->id }}/edit" class="col-xs-1 action-button testimonial-edit-button"><i class="fa fa-pencil"></i></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
