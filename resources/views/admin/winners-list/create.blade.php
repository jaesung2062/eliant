@extends('default')

@section('content')
    <div class="page admin-page page-admin-winners-list-create">
        <div class="container">
            @include('partials.admin-nav')
            @include('partials.alerts')
            @include('partials.validation')

            <h1 class="text-center">Create Award List</h1>

            <hr>

            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#simple" aria-controls="simple" role="tab" data-toggle="tab">Simple</a></li>
                    <li role="presentation"><a href="#categorized" aria-controls="categorized" role="tab" data-toggle="tab">Categorized</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="simple">
                        <form action="/admin/awards?type=simple" method="post" class="form-simple">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="name" class="col-xs-3 col-form-label text-right">Name</label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <span class="col-xs-3 col-form-label text-right">Class</span>
                                <div class="col-xs-9">
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="class" value="builder" {{ old('class') == 'builder' ? 'checked' : '' }}> Builder
                                    </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="class" value="individual" {{ old('class') == 'individual' ? 'checked' : '' }}> Individual
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-xs-3 col-form-label text-right">Description</label>
                                <div class="col-xs-9">
                                    <textarea class="form-control" id="description" name="description" placeholder="Description" rows="4">{{ old('description') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="winners" class="col-xs-3 col-form-label text-right">Winners</label>
                                <div class="col-xs-9 simple-winners-container sortable">
                                    <div class="form-group row winner-row">
                                        <div class="col-xs-6">
                                            <input type="text" class="form-control winner-input-name" name="winners[0][name]" placeholder="Winner Name">
                                        </div>
                                        <div class="col-xs-6">
                                            <input type="text" class="form-control winner-input-location" name="winners[0][location]" placeholder="Winner Location">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-9 col-xs-offset-3">
                                    <br>
                                    <button class="btn btn-success btn-add-winner-simple" type="button"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>

                            <div class="text-right row">
                                <div class="col-xs-9 col-xs-offset-3">
                                    <hr>
                                    <button class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="categorized">
                        <form action="/admin/awards?type=categorized" method="post" class="form-categorized">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="name" class="col-xs-3 col-form-label text-right">Name</label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <span class="col-xs-3 col-form-label text-right">Class</span>
                                <div class="col-xs-9">
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="class" value="builder" {{ old('class') == 'builder' ? 'checked' : '' }}> Builder
                                    </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="class" value="individual" {{ old('class') == 'individual' ? 'checked' : '' }}> Individual
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="winners" class="col-xs-3 col-form-label text-right">Winners</label>

                                <div class="col-xs-9 categories-container">
                                    <div class="category-container">
                                        <div class="form-group">
                                            <input type="text" class="form-control category-input" placeholder="Category">
                                        </div>

                                        <div class="category-winners-container">
                                            <div class="form-group row winner-row">
                                                <div class="col-xs-2">
                                                    <select name="winners[medium][0][place]" class="form-control">
                                                        <option value="">Place</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="h">Honorable</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control winner-input-name" name="winners[medium][0][name]" placeholder="Winner Name" data-index="0">
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control winner-input-location" name="winners[medium][0][location]" placeholder="Winner Location" data-index="0">
                                                </div>
                                            </div>
                                        </div>

                                        <button class="btn btn-default btn-add-winner-categorized" type="button">
                                            <i class="fa fa-plus"></i>
                                            Add New Winner
                                        </button>
                                    </div>
                                </div>

                                <div class="col-xs-9 col-xs-offset-3">
                                    <br>

                                    <button class="btn btn-success btn-add-category" type="button">
                                        <i class="fa fa-plus"></i>
                                        Add New Category
                                    </button>
                                </div>
                            </div>

                            <div class="text-right row">
                                <div class="col-xs-9 col-xs-offset-3">
                                    <hr>
                                    <button class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
