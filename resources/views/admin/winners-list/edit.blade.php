@extends('default')

@section('content')
    <div class="page admin-page page-admin-winners-list-edit">
        <div class="container">
            @include('partials.admin-nav')
            @include('partials.alerts')
            @include('partials.validation')

            <h1 class="text-center">Edit Awards</h1>

            <form action="/admin/awards/{{ $list->id }}" method="post" class="form-simple">
                {{ csrf_field() }}
                {{ method_field('patch') }}

                <div class="form-group row">
                    <label for="name" class="col-xs-3 col-form-label text-right">Name</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $list->name }}">
                    </div>
                </div>

                <div class="form-group row">
                    <span class="col-xs-3 col-form-label text-right">Class</span>
                    <div class="col-xs-9">
                        <label style="font-weight: normal;">
                            <input type="radio" name="class" value="builder" {{ (old('class') == 'builder' || $list->class == 'builder') ? 'checked' : '' }}> Builder
                        </label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label style="font-weight: normal;">
                            <input type="radio" name="class" value="individual" {{ (old('class') == 'individual' || $list->class == 'individual') ? 'checked' : '' }}> Individual
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-xs-3 col-form-label text-right">Description</label>
                    <div class="col-xs-9">
                        <textarea class="form-control" id="description" name="description" placeholder="Description" rows="4">{{ old('description') ?: $list->description }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="winners" class="col-xs-3 col-form-label text-right">Winners</label>
                    @if($list->type == 'simple')
                        <div class="col-xs-9 simple-winners-inputs-container sortable">
                            @foreach($list->winners as $i => $winner)
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" name="winners[{{ $i }}][name]" placeholder="Winner Name" value="{{ $winner['name'] }}">
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" name="winners[{{ $i }}][location]" placeholder="Winner Location" value="{{ $winner['location'] }}">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @elseif($list->type == 'categorized')
                        <div class="col-xs-9 categories-container">
                            @foreach($list->winners as $category => $winners)
                                <div class="category-container">
                                    <div class="form-group">
                                        <input type="text" class="form-control category-input" placeholder="Category" value="{{ $category }}">
                                    </div>

                                    <div class="category-winners-container">
                                        @foreach($winners as $i => $winner)
                                            <div class="form-group row winner-row" data-index="0">
                                                <div class="col-xs-2">
                                                    <select name="winners[medium][{{ $i }}][place]" class="form-control">
                                                        <option value="">Place</option>
                                                        <option value="1" {{ $winner['place'] == 1 ? 'selected' : '' }}>1</option>
                                                        <option value="2" {{ $winner['place'] == 2 ? 'selected' : '' }}>2</option>
                                                        <option value="3" {{ $winner['place'] == 3 ? 'selected' : '' }}>3</option>
                                                        <option value="h" {{ $winner['place'] == 'h' ? 'selected' : '' }}>Honorable</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control winner-input-name" name="winners[{{ $category }}][{{ $i }}][name]" placeholder="Winner Name" value="{{ $winner['name'] }}">
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control winner-input-location" name="winners[{{ $category }}][{{ $i }}][location]" placeholder="Winner Location" value="{{ $winner['location'] }}">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <button class="btn btn-default btn-add-winner-categorized" type="button">
                                        <i class="fa fa-plus"></i>
                                        Add New Winner
                                    </button>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <div class="col-xs-9 col-xs-offset-3">
                        <br>
                        <button class="btn btn-success btn-add-category" type="button">
                            <i class="fa fa-plus"></i>
                            Add New Category
                        </button>
                    </div>
                </div>

                <div class="text-right row">
                    <div class="col-xs-9 col-xs-offset-3">
                        <hr>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
