@extends('default')

@section('content')
    <div class="page admin-page page-admin-winners-list-index">
        <div class="container">
            @include('partials.admin-nav')
            @include('partials.alerts')

            <h1 class="text-center">Awards</h1>

            <div class="text-center">
                <a href="/admin/awards/create" class="btn btn-primary">Create</a>
                <br>
                <br>
            </div>

            <div class="row">
                @foreach($lists as $list)
                    <a href="/admin/awards/{{ $list->id }}/edit" class="sortable-entry col-xs-10" data-model-id="{{ $list->id }}">{{ $list->name }}</a>
                    <a href="#" class="col-xs-1 action-button award-delete-button" data-delete="{{ $list->id }}" data-resource="lists"><i class="fa fa-trash"></i></a>
                    <a href="#" class="col-xs-1 action-button award-edit-button"><i class="fa fa-pencil"></i></a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
