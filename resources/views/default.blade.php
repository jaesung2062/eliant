<!doctype html>
<!-- Microdata markup added by Google Structured Data Markup Helper. -->
<html lang="en">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-NNP4B3K');</script>
        <!-- End Google Tag Manager -->


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48638609-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-48638609-1');
        </script>
        {{--<!-- Global site tag (gtag.js) - Google Analytics -->--}}
        {{--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-48638609-1"></script>--}}
        {{--<script>--}}
        {{--window.dataLayer = window.dataLayer || [];--}}
        {{--function gtag(){dataLayer.push(arguments);}--}}
        {{--gtag('js', new Date());--}}

        {{--gtag('config', 'UA-108167539-1');--}}
        {{--</script>--}}
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ isset($meta) && isset($meta['title']) && $meta['title'] ? $meta['title'] : 'Eliant | Nationwide Customer Survey Company' }}</title>

        <meta name="description" content="{{ isset($meta) && isset($meta['description']) && $meta['description'] ? $meta['description'] : 'Anyone can ask questions, but asking the RIGHT ones is what gives us an edge in understanding your customers’ experience.' }}">
        <meta name="keywords" content="{{ isset($meta) && isset($meta['keywords']) && $meta['keywords'] ? $meta['keywords'] : 'market research, customer satisfaction' }}">
        <meta name="author" content="{{ isset($meta) && isset($meta['author']) && $meta['author'] ? $meta['author'] : 'Eliant, Inc.' }}">
        <meta name="geo.placename" content="27141 Aliso Creek Rd #290, Aliso Viejo, CA 92656, USA"/>
        <meta name="geo.position" content="33.5683460;-117.7239520"/>
        <meta name="geo.region" content="US-California"/>
        <meta name="ICBM" content="33.5683460, -117.7239520"/>
        <link rel="publisher" href="https://wbrandstudio.com/">
        <meta name="robots" content="noindex" />

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        @if(app()->isLocal())
            <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @else
            <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        @endif

        <script>
		Laravel = {csrfToken: '{{ csrf_token() }}'};
        </script>
    </head>
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNP4B3K"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        <div id="wrapper">
            @include('partials.nav')

            @include('partials.menu')

            @yield('content')


            @if(!Request::is('ratetrades'))
                @include('partials.footer')

            @elseif(Request::is('ratetrades'))
                @include('partials.ratetradefooter')
            @endif

        </div>

        @if(app()->isLocal())
            <script src="{{ asset('js/app.js') }}"></script>
        @else
            <script src="{{ mix('js/app.js') }}"></script>
        @endif
    </body>
</html>
