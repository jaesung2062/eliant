@extends('mobile')

@section('content')
    <div class="page page-about">
        <section>
            <img src="/images/mobile/about-page_03.jpg" alt="" class="section-image">

            <div class="container">
                <h1>We Were the First to Be Right.</h1>

                <p>We are the ORIGINAL homebuyer survey company for the new home industry. Eliant has been conducting intensive customer research and evaluations nationwide for over 30 years and has conducted over 2.5 million consumer surveys!</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/speaking-engagement.jpg" alt="" class="section-image">

            <div class="container">
                <h1>We ask the right questions. Not just questions.</h1>

                <p>All of our team members are extremely knowledgeable and trained on HOW to ask the right questions. HOW you ask? Our CEO Bob Mirman cut his teeth on designing psychological tests of immense complexity, you can say he is a doctor of ‘asking the right questions to find the right answers.’ You couldn’t ask for a better situation and you certainly won’t find it with any competitors who try to copy our business model.</p>
            </div>
        </section>

        <section>
            <div class="container">
                <img src="/images/mobile/about-eliant-history.jpg" alt="" class="section-image">

                <h1>When you do business with Eliant it just feels right.</h1>

                <p>Here's a list of some of our clients that have benefited from our game-changing customer evaluations over the years. Why not add yours to it?</p>

                <ul>
                    <li>BMW</li>
                    <li>General Mills</li>
                    <li>Toyota</li>
                    <li>Kraft</li>
                    <li>Procter and Gamble</li>
                    <li>Kellogg</li>
                    <li>Nabisco</li>
                    <li>Pontiac</li>
                    <li>General Foods</li>
                    <li>Beckman Instruments</li>
                    <li>IBM</li>
                    <li>HP</li>
                    <li>Emaar</li>
                    <li>Kinan in Saudi</li>
                </ul>
            </div>
        </section>
    </div>
@endsection
