@extends('mobile')

@section('content')
    <div class="page page-choice-awards-winners">
        <section>
            <img src="/images/mobile/homebuyers-choice-awards-page_03.jpg" alt="" class="section-image">

            <div class="container">
                <h1>Happy Customers are Truly Rewarding.</h1>

                <p>Receiving awards from your peers is prestigious, but being honored by your customers is the Holy Grail of Awesomeness. Over 20 years ago, we created the <em>Home Buyers's Choice Awards</em> to honor new home builders who busted their backsides to deliver the best experience to their customers by following Eliant's blueprint for success.</p>

                <p>You built a home for your customers. Did they build you up as the best? Come find out at the <em>Home Buyer's Choice Awards</em>.</p>
            </div>
        </section>
        <section>
            {{--<div class="video-container">--}}
                {{--<iframe src="https://www.youtube.com/embed/S2MQgqNyrDE" frameborder="0" allowfullscreen></iframe>--}}
            {{--</div>--}}

            <div class="container">
                <h1>2018 Homebuyer's Choice Awards</h1>

                <p>The following awards will be broken into three segments, based on sales volume: Medium Volume Builders (up to 125 homes), Large Volume Builders (126-275 homes) and High Volume Builders (over 276 homes).</p>

                <div class="text-center">
                    <br>
                    <a href="/choice-awards/winners" class="btn btn-lg btn-action">Winners List</a>
                    <br>
                    <br>
                    <a href class="btn btn-lg btn-action">Become a sponsor</a>
                    <br>
                    <br>
                </div>
            </div>
        </section>

    </div>
@endsection
