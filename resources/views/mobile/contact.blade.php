@extends('mobile')

@section('content')
    <div class="page page-contact">
        <section>
            <img src="/images/mobile/contact.jpg" alt="" class="section-image">

            <div class="container">
                <h1>We're Ready to Answer <span class="text-theme">the Call, Tweet, FB message... or Email.</span></h1>

                <p>Give us a shout and let’s build your success together. Our staff psychologist and account managers and are waiting to create your next questionnaires, interpret your customers’ feedback, and help create an Action Plan for delivering an extraordinary customer experience.</p>
            </div>
        </section>

        <section>
            @include('partials.alerts')
            <br>
            @include('partials.contact-form', ['buttonText' => 'Contact Eliant'])
            <br>
            <br>
            <br>
            <br>
        </section>
    </div>
@endsection
