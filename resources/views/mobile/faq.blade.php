@extends('mobile')

@section('content')
    <script>
    var faqs = {!! json_encode($faqs) !!};
    </script>

    <div class="page page-faq">
        <header class="section-header bg-gradient contains-animations">
            <div class="container">
                <h1>Your Question of Success is Answered Here.</h1>

                <p>We help our clients sell more products & services by providing the right answers that lead to the best results. And if you don't know which questions to ask no worries. Our highly-trained account managers (led by our expert psychologist CEO) know how to ask the right questions to get the ball rolling and your business building.</p>
            </div>
        </header>

        <section class="section-search bg-theme text-center">
            <h1>FAQ</h1>

            <label for="faq-search-input">Find answers to frequently asked questions here.</label>

            <div class="search-input-container">
                <input type="search" id="faq-search-input" class="form-control">
                <i class="fa fa-search" id="faq-search-input-submit"></i>
            </div>
        </section>

        <section class="section-questions bg-gradient">
            <div class="container">
                <h2 class="none-found-message text-center" style="display: none;">Sorry we couldn't find related questions.</h2>

                <div class="answers-container"></div>
            </div>
        </section>
    </div>
@endsection
