@extends('default')

@section('content')
    <div class="page page-surveys">
        <header class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="text-container text-left">


                            <h4>2018 Homebuyers’<br>Choice Awards</h4>

                            <p>This event includes a networking hour, lunch and then we will announce the winners of the 22nd Annual HomeBuyers' Choice Awards. Attire is business casual and this event is invitation-only.</p>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <section class="section-answers contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 image-column" style="margin-top: 10%;">
                        <img src="/images/HBC_Final_version_gradient_RGB.png" class="img-responsive" style="margin-top: 75px; margin-bottom: 75px; width:80%;" alt="">
                    </div>
                    <div class="col-sm-6 copy-column text-container">
                        <h1 class="" style="color: #00adbb;">SAVE THE DATE!</h1>
<br><br>
                        <p style="font-size: 16pt;"><strong>WHEN:</strong><br>Tuesday, February 20th, 2018<br>11:00 am - 2:30 pm</p>
                     <br>
                        <p style="font-size: 16pt;"><strong>WHERE:</strong><br>Seven-Degrees<br>891 Laguna Canyon Road<br>Laguna Beach, CA 92651</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-experience contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-center text-container">
                        <h3 style="margin-top: 100px; ">Tickets All Sold Out</h3>
                        <p style="margin-bottom: 75px;">For further questions please contact hbc@eliant.com</p>


                </div>
            </div>
            </div>
        </section>

    </div>
    </div>
@endsection
