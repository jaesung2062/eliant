@extends('mobile')

@section('content')
    <div class="page page-home">
        <section>
            <img src="/images/mobile/homepage-main-image.jpg" alt="" class="section-image">

            <div class="container">
                <h1>Providing Our Clients with the <span class="text-theme">Right Answers</span>.</h1>

                <p>Answers to questions about your customers’ experience don’t mean squat unless they’re right. And they’re right only if they lead to greater sales success. For over 30 years, <a href="/surveys"><b class="text-theme">Eliant</b></a> has provided our clients with deep insights about their customers…the right answers to help them grow their business beyond expectations.</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/eliant-surveys.jpg" alt="" class="section-image">

            <div class="container">
                <h1>The Eliant Surveys. <span class="text-theme">Experience Better</span>.</h1>

                <p><a href="/surveys" class="text-theme"><b>The Eliant Surveys</b></a> are the heart of the company that started it all. Anyone can ask questions, but asking the RIGHT ones is what gives us an edge in understanding your customers’ experience. Getting evaluation results you can trust from the original homebuyer survey company is the best way to experience a world of better.</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/get-social.jpg" alt="" class="section-image">

            <div class="container">
                <h1>Get Social. Give Answers.</h1>

                <p>Turn your customers’ positive comments into praising tweets, posts and pics with <a href="/surveys#rateSOCIAL" class="text-theme"><b>rateSOCIAL</b></a>. The world relies on social media and so should you. Partner with Eliant and rateSOCIAL and we’ll help you build a social media marketing plan that will send shockwaves through the twitterverse and blogosphere and deliver a whole new level of success for your business. No one knows more about your company than your customers.</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/certification.jpg" alt="" class="section-image">

            <div class="container">
                <h1>Certify Your Team in Customer Awesomeness.</h1>

                <p>The <a href="/services#eliant-certification" class="text-theme"><b>Eliant Certification Program</b></a> was designed to motivate your employees to become customer service ninjas. It’s an incredibly effective way to evaluate and recognize your team’s performance. The program allows you to hold your team members accountable for consistently delivering an incredible “OMG” customer experience.</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/awards.jpg" alt="" class="section-image">

            <div class="container">
                <h1>See if Homebuyers Chose YOU.</h1>

                <p>One of the most anticipated events in the home building industry is the annual <a href="/choice-awards" class="text-theme"><b>Home Buyers Choice Awards</b></a>. This is a gala award ceremony that takes place in a magnificent venue near Eliant’s national headquarters.</p>
            </div>
        </section>

    </div>
@endsection