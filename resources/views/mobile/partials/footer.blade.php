<footer id="footer" class="text-center">
    {{--<p>--}}
        {{--&copy; Copyright Eliant 2017 <br>--}}
        {{--Eliant, Inc. <br>--}}
        {{--27141 Alisio Creek, Suite 290 <br>--}}
        {{--Alisio Viejo, CA 92656--}}
    {{--</p>--}}

    <a href="https://www.linkedin.com/company/68026?trk=tyah&trkInfo=tas%3Aeliant%2C%20" target="_blank">
        <i class="fa fa-linkedin"></i>
    </a>

    &nbsp;&nbsp;&nbsp;&nbsp;

    <a href="https://www.facebook.com/Eliantinc" target="_blank">
        <i class="fa fa-facebook"></i>
    </a>

    &nbsp;&nbsp;&nbsp;&nbsp;

    <a href="https://www.youtube.com/channel/UCFRCVl4zMevDMwZfG0ge8Sw" target="_blank">
        <i class="fa fa-youtube-play"></i>
    </a>
</footer>