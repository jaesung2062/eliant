<nav id="menu">
    <a href="#" class="menu-close-button">Close (&times;)</a>

    <a href="/about" class="menu-link">About</a>
    <a href="/testimonials" class="menu-link">Testimonials</a>
    <a href="/blog" class="menu-link">The Eliant Blog</a>
    <a href="/surveys" class="menu-link">Surveys</a>
    <a href="/services" class="menu-link">Services</a>
    <a href="/faq" class="menu-link">FAQ</a>
    <a href="/choice-awards" class="menu-link">Homebuyers Choice Award</a>
    <a href="/contact" class="menu-link">Contact</a>
</nav>
