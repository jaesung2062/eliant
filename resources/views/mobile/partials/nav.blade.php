<nav id="navbar">
    <div class="container-fluid">
        <div class="row">
            <a href="/" class="col-xs-6 logo-container">
                <img src="/images/eliant-logo.png" alt="Eliant Logo" class="nav-logo img-responsive center-block">
            </a>
            <div class="col-xs-6 right-column">
                <div class="row">
                    <a href="#" class="col-xs-12 menu-dropdown-button vertical-center-content text-center">
                        <img src="/images/mobile/menu-icon.png" alt="" style="display: inline-block;">
                        <span>MENU</span>
                    </a>
                </div>
                <div class="row max-height text-center">
                    <a href="tel:866-755-8199" class="col-xs-6 phone-button vertical-center-content">
                        <img src="/images/mobile/phone-icon.png" alt="">
                    </a>
                    <a href="mailto:answers@eliant.com" class="col-xs-6 contact-button vertical-center-content">
                        <img src="/images/mobile/email-icon.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
