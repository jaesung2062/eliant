@extends('default')
{{--Test--}}
@section('content')
    <div class="page page-about">
        <section class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            {{--<h4 class="subheading">About Eliant</h4>--}}
                            <h3 style="color: #71bf44;">rateTRADES™ builders review of sub-contractors.</h3>

                            <p>Superintendents, customer service representatives, and purchasing agents evaluate the work of each trade (sub-contractor). Eliant uses this input to rank your trades against other similar trades in the market. Eliant also offers the TradeStar program that allows builders to easily share their ratings with their trade partners. Now, subcontractors can view builder feedback and then formulate and execute specific action plans. Contact us below for more information.</p>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/ratetrade_images/top-left-ratetrades.jpg" alt="" class="img-responsive grid-image image-about-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/ratetrade_images/top-right-ratetrades.jpg" alt="" class="img-responsive grid-image image-onsite-2">
                    </div>
                </div>
            </div>
        </section>





        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    @include('partials.alerts')
                    <br>
                    @include('partials.contact-form', ['buttonText' => 'Contact Eliant'])
                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
