@extends('mobile')

@section('content')
    <div class="page page-services">
        <section>
            <img src="/images/mobile/services-main.jpg" alt="" class="section-image">

            <div class="container">
                <h1>The Answer to Your Success is More Than Customer Surveys.</h1>

                <p>Eliant is all about providing continuous improvement insights to our clients. Our services and real-time report data offer a deeper and clearer look into what your customers are thinking. Now you can have a better understanding of how to make their experience better and how to get that edge on your competition.</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/speaking-engagement.jpg" alt="" class="section-image">

            <div class="container">
                <h1>Speaking Engagements. Not the Same Old Show.</h1>

                <p>As Eliant’s founder, CEO, and psychologist, Bob Mirman’s international presentations focus on the psychological and commercial aspects of the homebuyers’ customer experience. Bob provides humor and invaluable insight into interpreting survey scores and rankings and is renowned as a keynote speaker at corporate and building-industry events such as PCBC, IBS, ECBC, and BIS show.</p>
                <p>Jex Manwaring, Eliant’s president since 2014, has served as an executive of large multi-divisional builder. His ‘Action Planning Workshops’ are widely acclaimed as the best in the industry! <a href="/contact"><b>Inquire within</b></a>.</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/training.png" alt="" class="section-image">

            <div class="container">
                <h1>Get Training & Deliver the "OMG" Factor</h1>

                <p>The first step is upgrading the behavior of everyone on your team so they can consistently “delight” the customer and exceed expectations, all while holding them accountable for delivering the “OMG” factor. Our step-by-step ‘Customer Experience Blueprint’ make this easy. Most training programs just impart information, our training is designed to improve and transform your teams’ behaviors…and your ratings. <a href="/contact"><b>Get started today</b></a>.</p>
            </div>
        </section>

        <section>
            <img src="/images/mobile/certification-main.jpg" alt="" class="section-image">

            <div class="container">
                <h1>Eliant Certification. Build Your Competitive Edge.</h1>

                <p>The Eliant Certification Program rewards your employees for upping their game when they improve their performance. But what really makes this program unique are the psychological techniques developed by our clinical psychologist and CEO, Bob Mirman. These techniques take a deeper, more methodical approach to asking the right questions in order to deliver the right answers. <br> <a href="/contact"><b>Contact us</b></a> for more information.</p>
            </div>
        </section>
    </div>
@endsection
