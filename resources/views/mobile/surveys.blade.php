@extends('mobile')

@section('content')
    <div class="page page-surveys">
        <section>
            <img src="/images/mobile/eliant-surveys-main.jpg" alt="" class="section-image">

            <div class="container">
                <h1>The Eliant Surveys. Experience Better.</h1>

                <p>The Eliant Surveys provide an accurate, in-depth and timely analysis of customer evaluations regarding their overall experience with your company and specific behaviors of your team members. So, you’ll see the areas where you kick butt and where you’re getting your butt kicked.</p>
            </div>
        </section>
        <section>
            <img src="/images/mobile/got-customers.jpg" alt="" class="section-image">

            <div class="container">
                <h1>Got customers? We’ve got all the answers.</h1>

                <p>A major division of Eliant is dedicated to the real estate industry, but we evaluate customer perceptions for many other industries as well. No matter what field you’re in, we can collect customer evaluations, which provide the right answers to help your business grow in ways you never thought possible. The Eliant Surveys offer products to fit any industry.</p>
            </div>
        </section>
        <section>
            <img src="/images/mobile/rateExperience.jpg" alt="" class="section-image">

            <div class="container">
                <h1 style="color: #F8931D">rateEXPERIENCE - surveys for any industry.</h1>

                <p>No matter what your business, if you have customers we have a customer experience review process, which can be customized for your specific industry. Our evaluations provide valuable data to give you a deeper insight into what prospective customers want and how well you’re meeting their needs. Keep them delighted. Keep them coming back. Keep getting referrals. <a href="/contact"><b>Contact us</b></a> to see how your business can benefit from our surveys.</p>

                <div class="text-center logo-image-container">
                    <img src="/images/mobile/rateExperience-logo.jpg" alt="" class="img-responsive center-block">
                </div>
            </div>
        </section>
        <section>
            <img src="/images/mobile/ratehome.jpg" alt="" class="section-image">

            <div class="container">
                <h1 style="color: #0FC4CF">rateHOME - the #1 evaluation program for new home builders.</h1>

                <p>With over 2 million surveys to date, rateHOME (previously the Eliant Builder Survey – note: check the name and insert) is the premier evaluation program for new home builders nationwide. Our 2016 average survey response rate was over 70%! Now that’s something to write home about. Want more info about rateHOME? <a href="/contact"><b>Call or email us today.</b></a></p>

                <div class="text-center logo-image-container">
                    <img src="/images/mobile/ratehome-logo.jpg" alt="" class="img-responsive center-block">
                </div>
            </div>
        </section>
        <section>
            <img src="/images/mobile/rateLender.jpg" alt="" class="section-image">

            <div class="container">
                <h1 style="color: #EB3124">rateLENDER - the perfect answer for lenders.</h1>

                <p>Finding the money to buy a new home is probably the hardest step for new home buyers. With rateLENDER, you’ll be able to see the customers’ rating of their experience in getting that perfect loan. Want answers? <a href="/contact"><b>Ask us</b></a>.</p>

                <div class="text-center logo-image-container">
                    <img src="/images/mobile/rateLender-logo.jpg" alt="" class="img-responsive center-block">
                </div>
            </div>
        </section>
        <section>
            <img src="/images/mobile/ratetrades.jpg" alt="" class="section-image">

            <div class="container">
                <h1 style="color: #65D00E">rateTRADES - builders review of sub-contractors.</h1>

                <p>Superintendents, customer service representatives, and purchasing agents evaluate the work of each trade (sub-contractor). Eliant uses this input to rank the trades you typically use and against other similar trades in the market. Eliant also offers the TradeStar program that allows builders to easily share their ratings with their trade partners. Now, subcontractors can view builder feedback and then formulate and execute specific action plans.</p>

                <div class="text-center logo-image-container">
                    <img src="/images/mobile/rateTrades-logo.jpg" alt="" class="img-responsive center-block">
                </div>
            </div>
        </section>
        <section>
            <img src="/images/mobile/rateBuilder.jpg" alt="" class="section-image">

            <div class="container">
                <h1 style="color: #B59F6E">rateBUILDER - exclusive to Eliant for new home builders.</h1>

                <p>Sub-contractors can now rate new home/condo/apartment builders based on their experience. Builders can then see their own ratings and how they match up against other builders in the area. Want to be reviewed better than your competitors? <a href="/contact"><b>Ask us how</b></a>.</p>

                <div class="text-center logo-image-container">
                    <img src="/images/mobile/rateBuilder-logo.jpg" alt="" class="img-responsive center-block">
                </div>
            </div>
        </section>
        <section>
            <img src="/images/mobile/rateSocial.jpg" alt="" class="section-image">

            <div class="container">
                <h1 style="color: #A95DFD">rateSOCIAL</h1>

                <p>Don’t know if you’ve heard, but social media is kind of a big deal these days. So, leverage it with rateSOCIAL. Allow your prospective buyers to see what your current customers have to say about you! Now all of your customer feedback posts can be linked to your Facebook and Twitter accounts to get real-time evaluation data, increase website sales and improve Google search results. <a href="/contact"><b>Contact us via Facebook for the social experience, or just call</b></a>.</p>

                <div class="text-center logo-image-container">
                    <img src="/images/mobile/rateSocial-logo.jpg" alt="" class="img-responsive center-block">
                </div>
            </div>
        </section>
    </div>
@endsection
