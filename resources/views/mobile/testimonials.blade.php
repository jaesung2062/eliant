@extends('mobile')

@section('content')
    <div class="page page-testimonials">
        <section>
            <img src="/images/mobile/testimonials.jpg" alt="" class="section-image">

            <div class="container">
                <h1>See Our Clients’ Answers Before You Ask for Ours.</h1>

                <p>We’ve helped many clients just like you, with questions just like yours. Listen to what real people are saying about our answers, and when you’re ready we’ll get some for <a href="#">you, too</a>.</p>
            </div>
        </section>

        <section class="testimonials-container">
            @foreach($testimonials as $testimonial)
                <div class="testimonial">
                    <img src="{{ $testimonial->getPersonImagePath() }}" alt="" class="img-responsive">

                    <blockquote>"{{ $testimonial->quote }}"</blockquote>

                    <p class="quote-info">
                        {{ $testimonial->person }} <br>
                        {{ $testimonial->company }}
                    </p>

                    @if($testimonial->getCompanyImagePath())
                        <img src="{{ $testimonial->getCompanyImagePath() }}" alt="" class="img-responsive">
                    @endif
                </div>
            @endforeach
        </section>
    </div>
@endsection
