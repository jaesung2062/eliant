@extends('default')
{{--Test--}}
@section('content')
    <div class="page page-about">
        <section class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            <h4 class="subheading">About Eliant</h4>
                            <h3>We Were the First to be Right.</h3>

                            <p>We are the ORIGINAL homebuyer survey company for the new home industry. Eliant has been conducting intensive customer research and evaluations nationwide for over 34 years and has conducted over 2.5 million consumer surveys!</p>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/about-page-desktop-Eliant-Sept-11-2017_03.jpg" alt="" class="img-responsive grid-image image-about-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/about-page-desktop-Eliant-Sept-11-2017_06.jpg" alt="" class="img-responsive grid-image image-about-2">
                    </div>
                </div>
            </div>
        </section>

        <section class="section-history contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <img src="/images/_about-page-desktop-Eliant-Sept-11-2017_06.jpg" alt="" class="img-responsive grid-image crowd-1">
                    </div>

                    <div class="col-xs-3">
                        <img src="/images/_about-page-desktop-Eliant-Sept-11-2017_03.jpg" alt="" class="img-responsive grid-image crowd-2">
                    </div>

                    <div class="col-xs-6">
                        <div class="copy-container on-right-side">
                            <h1>We ask the right questions. Not just questions.</h1>

                            <p>All of our team members are extremely knowledgeable and trained
                                on HOW to ask the right questions. HOW you ask? Our CEO Bob Mirman cut his teeth on designing psychological tests of immense complexity. You can say he is a doctor of ‘asking the right questions to find the right answers.’ You couldn’t ask for a better situation and you certainly won’t find it with any competitors who try to copy our business model.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="section-business">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 title-container text-center">
                        <h1>When you do business with Eliant it just feels right.</h1>

                        <br>
                        <p>Our Eliant team assists clients in many industries. Here’s a list of some of our clients that have benefited from our game-changing customer evaluations. Why not add yours to it?</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="client-logos bg-gradient-reverse">
                <div class="container logos-container contains-animations">
                    <div class="row">
                        <div class="client-logos-column col-xs-3">
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logo-bmw.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_26.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_34.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logo-general-mills.jpg" alt="" class="img-responsive"></div>
                        </div>

                        <div class="client-logos-column col-xs-3">
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logo-kraft.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_29.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_37.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/kinan-logo_03.jpg" alt="" class="img-responsive"></div>

                        </div>

                        <div class="client-logos-column col-xs-3">
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logo-toyota.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_32.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_39.jpg" alt="" class="img-responsive"></div>
                        </div>

                        <div class="client-logos-column col-xs-3">
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_48.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_51.jpg" alt="" class="img-responsive"></div>
                            <div class="client-logo-container slide-in-up"><img src="/images/client-logos_53.jpg" alt="" class="img-responsive"></div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
@endsection
