@extends('default')

@section('content')

    <?php $authors = array("Eliant", "Admin", "Max Mirman", "Ben Classen", "Eric Mitchell", "Gwenith Ramirez", "Bob Mirman", "Eliant", "Eliant") ?>
    <div class="page page-blog-index">
        <header class="section-header bg-gradient contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container on-left-side">
                            <h4 class="subheading">Eliant Blog</h4>

                            <h1>The Eliant Brain Dump.</h1>

                            <p>
                                This is where we transfer accessible knowledge from our brains to yours. Get all the
                                inside info from our knowledgeable staff, industry experts and customers just like you.
                                Our always-growing collection of interesting and informative articles is overflowing
                                with incredible insights and the right answers to all your questions.
                            </p>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/blog-page-desktop-Eliant-Sept-11-2017_06.jpg" alt="" class="image-column-1 img-responsive">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/blog-page-desktop-Eliant-Sept-11-2017_03.jpg" alt="" class="image-column-2 img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="section-blog-list">
            <div class="container">
                <div class="row">
                    <div class="col-xs-9">
                        @foreach($posts as $post)
                            <div class="row blog-listing">
                                <div class="col-xs-3 image-container vertical-center-container">
                                    {{--<img src="{{ $post->getImageAttribute() }}" style="height: 150px;width: 150px;" alt="">--}}
                                    {{--<img src="{{ $post->image }}" style="height: 150px;width: 150px;" alt="">--}}
                                    <img src="{{ asset('images/eliant-logo.png') }}" style="height: 70px;width: 150px;" alt="">

                                </div>
                                <div class="col-xs-9 description-container">
                                    <h3><a href="/blog/{{ $post->ID }}/{{ str_slug($post->title) }}">{{ $post->title }}</a></h3>

                                    <p>{{ $post->post_date->format('F jS, Y') }}</p>
                                    <p>{{ $authors[$post->post_author] }}</p>
                                    <p>{{ $post->post_excerpt }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="col-xs-3" style="padding: 0;">
                        <div class="subscribe-container bg-theme">
                            <h4>Subscribe to the Eliant Blog</h4>

                            <p>
                                Get all the inside info from our knowledgeable staff, industry experts and customers
                                just like you from the Eliant Blog. Our always-growing brain dump of interesting and
                                informative content is overflowing with incredible insights, interesting facts, and the
                                right answers to all your questions.
                            </p>



                            <link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
                            <form action="https://eliant.us17.list-manage.com/subscribe/post?u=9ede124ae5f81e9125b1372c4&amp;id=aa24b1a640" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="email" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="* YOUR EMAIL">
                                </div>
                                <div class="text-center">
                                    <div class=""><input class="btn btn-action btn-block" type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>


                                </div>
                            </form>

                            {{--<form action="/blog" method="post">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" name="submail" class="form-control" placeholder="* YOUR EMAIL">--}}
                                {{--</div>--}}
                                {{--<div class="text-center">--}}
                                    {{--<button class="btn btn-action btn-block">Subscribe</button>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        </div>

                        @include('partials.featured-posts')
                    </div>
                </div>
                <div class="text-center">
                    {!! $posts->links(); !!}
                </div>
            </div>
        </section>
    </div>
@endsection
