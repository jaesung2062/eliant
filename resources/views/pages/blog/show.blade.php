@extends('default')

@section('content')

    <?php $authors = array("Eliant", "Admin", "Max Mirman", "Ben Classen", "Eric Mitchell", "Gwenith Ramirez", "Bob Mirman", "Eliant", "Eliant") ?>
    <div class="page page-blog-show">
        <header class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            <h4>ELIANT ARTICLES, NEWS, RESEARCH, BLOG</h4>

                            <h1>{{ $post->title }}</h1>

                            <p class="blog-post-date text-uppercase">{{ $post->post_date->format('F jS, Y') }}</p>
                            <p>{{ $authors[$post->post_author] }}</p>
                            <div class="share-links">
                                <a href="http://www.facebook.com/share.php?u=http://example.com&title=my+title"><i class="fa fa-3x fa-facebook-square"></i></a>
                                <a href="#"><i class="fa fa-3x fa-twitter-square"></i></a>
                                <a href="#"><i class="fa fa-3x fa-linkedin-square"></i></a>
                                <a href="#"><i class="fa fa-3x fa-email"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6"><img src="/images/blog-show-image_03.jpg" alt="" class="grid-image"></div>
                            <div class="col-xs-6">
                                <div class="bg-theme grid-image subscribe-container">
                                    <p>Subscribe to Eliant Blog</p>

                                    <p>
                                        Get all the inside info from our knowledgeable staff, industry experts and
                                        customers just like you from the Eliant Blog. Our always-growing brain dump of
                                        interesting and informative content is overflowing with incredible insights,
                                        interesting facts, and the right answers to all your questions.
                                    </p>

                                    <link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
                                    <form action="https://eliant.us17.list-manage.com/subscribe/post?u=9ede124ae5f81e9125b1372c4&amp;id=aa24b1a640" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="email" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="* YOUR EMAIL">
                                        </div>
                                        <div class="text-center">
                                            <div class=""><input class="btn btn-action btn-block" type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="section-blog-post">
            <div class="container">
                <div class="row">
                    <div class="col-xs-9">
                        <article class="blog-post">{!! $post->post_content !!}</article>
                    </div>
                    <div class="col-xs-3" style="padding-left: 3px;padding-right: 3px;">
                        <div class="featured-posts-container">
                           @include('partials.featured-posts', ['posts' => $featuredPosts])

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
