@extends('default')

@section('content')
    <div class="page page-choice-awards-winners">
        <header class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container">
                            <h4 class="subheading">WINNERS & CRITERIA</h4>
                            <h1>And the Winner is...</h1>

                            <p>
                                Homebuilders who provided their customers with the best purchase and ownership
                                experiences-according to surveyed homebuyers-have been honored in the 22nd annual Eliant
                                Homebuyers' Choice Awards competition. The winning builders were chosen based on the
                                results of more than 106,000 surveys which were administered to all recent home owners
                                from over 156 major homebuilders across the U.S.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/winners-page-desktop_03.jpg" alt="" class="img-responsive grid-image image-column-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/winners-page-desktop_06.jpg" alt="" class="img-responsive grid-image image-column-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="section-intro contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/winners-list-img_03.jpg" class="grid-image image-column-1" alt="">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/winners-list-img_06.jpg" class="grid-image image-column-2" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="copy-container on-right-side">
                            <h1>2018 Homebuyers' Choice Awards Winner Celebration</h1>

                            <p>
                                Winners of the "Eliant 2018 Home Buyers' Choice" awards are based only on scores from
                                all surveys received from January 1-December 31, 2017, regardless of closing date.
                            </p>

                            <p>
                                The 2018 Homebuyers' Choice Award luncheon to honor our award winners.
                            </p>

                            <br>
                            <br>

                            <div class="row example-award-container">
                                <div class="col-xs-6">
                                    {{--<img src="/images/eliant-award-sample.png" alt="" class="img-responsive center-block">--}}
                                    <h4><b>When:</b></h4>
                                    <p>Tuesday, February 20th, 2018</p>
                                    <p>11:00 am - 2:30 pm</p>
                                    <br>
                                    <h4><b>Where:</b></h4>
                                    <p>Seven-Degrees</p>
                                    <p>891 Laguna Canyon Road</p>
                                    <p>Laguna Beach, CA 92651</p>
                                    <br><br>
                                    <a href="/images/winners_list_2018.pdf" class="btn btn-lg btn-action" style="margin-right: 4rem;" target="_blank">Winners List</a>
                                </div>
                                <div class="col-xs-6">
                                    {{--<h4><b>When:</b></h4>--}}
                                    {{--<p>Tuesday, February 20th, 2018</p>--}}
                                    {{--<p>11:00 am - 2:30 pm</p>--}}
                                    {{--<br>--}}
                                    {{--<h4><b>Where:</b></h4>--}}
                                    {{--<p>Seven-Degrees</p>--}}
                                    {{--<p>891 Laguna Canyon Road</p>--}}
                                    {{--<p>Laguna Beach, CA 92651</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
        {{--<section class="section-winners-builders contains-animations">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="copy-container on-left-side">--}}
                            {{--<h1>Top Builder Award Categories</h1>--}}

                            {{--<p>Click on category to see each winner.</p>--}}

                             {{--<ul>--}}
                                {{--@foreach($builderWinnersLists as $list)--}}
                                    {{--<li class="awards-listing">--}}
                                        {{--<div>--}}
                                            {{--<a href="#" class="winner-list-modal-link" data-toggle="modal" data-target="[data-modal-list='{{ $list->id }}']">--}}
                                                {{--{{ $list->name }}--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--@include('partials.modals.winners-list', compact('list'))--}}
                                {{--@endforeach--}}
                               {{----}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="row low-margin">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_14.jpg" class="grid-image image-column-1" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_11.jpg" class="grid-image image-column-2" alt="">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}

        {{--<section class="section-winners-individuals contains-animations">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="row low-margin">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_19.jpg" class="grid-image image-column-2" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_22.jpg" class="grid-image image-column-1" alt="">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="copy-container on-right-side">--}}
                            {{--<h1>Top Individual Award Categories</h1>--}}

                            {{--<p>Click on category to see each winner.</p>--}}

                            {{--<div class="row">--}}
                                {{--<ul>--}}
                                    {{--@foreach($individualWinnersLists as $list)--}}
                                        {{--<li class="awards-listing">--}}
                                            {{--<a href="#" class="winner-list-modal-link" data-toggle="modal" data-target="[data-modal-list='{{ $list->id }}']">--}}
                                                {{--{{ $list->name }}--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--@include('partials.modals.winners-list', compact('list'))--}}
                                    {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
    </div>
@endsection

{{--ORIGINAL WINENRS PAGE BELOW, NEW CHANGED ONE ABOVE--}}
{{--@extends('default')--}}

{{--@section('content')--}}
    {{--<div class="page page-choice-awards-winners">--}}
        {{--<header class="section-header contains-animations">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="text-container">--}}
                            {{--<h4 class="subheading">WINNERS & CRITERIA</h4>--}}
                            {{--<h1>And the Winner is...</h1>--}}

                            {{--<p>--}}
                                {{--Homebuilders who provided their customers with the best purchase and ownership--}}
                                {{--experiences-according to surveyed homebuyers-have been honored in the 22nd annual Eliant--}}
                                {{--Homebuyers' Choice Awards competition. The winning builders were chosen based on the--}}
                                {{--results of more than 106,000 surveys which were administered to all recent home owners--}}
                                {{--from over 156 major homebuilders across the U.S.--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="row low-margin">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/winners-page-desktop_03.jpg" alt="" class="img-responsive grid-image image-column-1">--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/winners-page-desktop_06.jpg" alt="" class="img-responsive grid-image image-column-2">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</header>--}}

        {{--<section class="section-intro contains-animations">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="row low-margin">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_03.jpg" class="grid-image image-column-1" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_06.jpg" class="grid-image image-column-2" alt="">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="copy-container on-right-side">--}}
                            {{--<h1>2018 Homebuyers' Choice Awards Winner Celebration</h1>--}}

                            {{--<p>--}}
                                {{--Winners of the "Eliant 2018 Home Buyers' Choice" awards are based only on scores from--}}
                                {{--all surveys received from January 1-December 31, 2016, regardless of closing date.--}}
                            {{--</p>--}}

                            {{--<p>--}}
                                {{--The 2018 Homebuyers' Choice Award luncheon to honor our award winners.--}}
                            {{--</p>--}}

                            {{--<br>--}}
                            {{--<br>--}}

                            {{--<div class="row example-award-container">--}}
                                {{--<div class="col-xs-6">--}}
                                    {{--<img src="/images/eliant-award-sample.png" alt="" class="img-responsive center-block">--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-6">--}}
                                    {{--<h4><b>When:</b></h4>--}}
                                    {{--<p>Tuesday, February 20th, 2018</p>--}}
                                    {{--<p>11:00 am - 2:30 pm</p>--}}
                                    {{--<br>--}}
                                    {{--<h4><b>Where:</b></h4>--}}
                                    {{--<p>Seven-Degrees</p>--}}
                                    {{--<p>891 Laguna Canyon Road</p>--}}
                                    {{--<p>Laguna Beach, CA 92651</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}

        {{--<section class="section-winners-builders contains-animations">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="copy-container on-left-side">--}}
                            {{--<h1>Top Builder Award Categories</h1>--}}

                            {{--<p>Click on category to see each winner.</p>--}}

                            {{--<ul>--}}
                                {{--@foreach($builderWinnersLists as $list)--}}
                                    {{--<li class="awards-listing">--}}
                                        {{--<div>--}}
                                            {{--<a href="#" class="winner-list-modal-link" data-toggle="modal" data-target="[data-modal-list='{{ $list->id }}']">--}}
                                                {{--{{ $list->name }}--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--@include('partials.modals.winners-list', compact('list'))--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="row low-margin">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_14.jpg" class="grid-image image-column-1" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_11.jpg" class="grid-image image-column-2" alt="">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}

        {{--<section class="section-winners-individuals contains-animations">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="row low-margin">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_19.jpg" class="grid-image image-column-2" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/images-winners-list_22.jpg" class="grid-image image-column-1" alt="">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="copy-container on-right-side">--}}
                            {{--<h1>Top Individual Award Categories</h1>--}}

                            {{--<p>Click on category to see each winner.</p>--}}

                            {{--<div class="row">--}}
                                {{--<ul>--}}
                                    {{--@foreach($individualWinnersLists as $list)--}}
                                        {{--<li class="awards-listing">--}}
                                            {{--<a href="#" class="winner-list-modal-link" data-toggle="modal" data-target="[data-modal-list='{{ $list->id }}']">--}}
                                                {{--{{ $list->name }}--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--@include('partials.modals.winners-list', compact('list'))--}}
                                    {{--@endforeach--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
    {{--</div>--}}
{{--@endsection--}}
