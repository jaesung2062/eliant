@extends('default')

@section('content')
    <div class="page page-choice-awards">
        <header class="section-header contains-animations bg-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container">
                            <h4 class="subheading">HomeBuyers' Choice Awards</h4>
                            <h1>Delighted Customers are Truly Rewarding.</h1>
                            

                            <p>Receiving awards from your peers is prestigious, but being honored by your customers is the Holy Grail of Awesomeness. Over 20 years ago, we created the <em>Home Buyers's Choice Awards</em> to honor new home builders who busted their backsides to deliver the best experience to their customers by following Eliant's blueprint for success.</p>

                            <p>You built a home for your customers. Did they build you up as the best? Come find out at the <em>Home Buyer's Choice Awards</em>.</p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/homebuyers-choice-awards_03.jpg" alt="" class="img-responsive grid-image image-column-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/homebuyers-choice-awards_06.jpg" alt="" class="img-responsive grid-image image-column-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="bg-gradient">
            {{--<section class="section-video text-center">--}}
                {{--<div class="container">--}}
                    {{--<div class="video-container">--}}
                        {{--<iframe src="https://www.youtube.com/embed/S2MQgqNyrDE" frameborder="0" allowfullscreen></iframe>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section>--}}

            <section class="section-choice-awards text-center">
                <div class="container">
                    <h1>2018 Homebuyer's Choice Awards</h1>

                    <p>The following awards will be broken into three segments, based on sales volume: Medium Volume Builders (up to 125 homes), Large Volume Builders (126-275 homes) and High Volume Builders (over 276 homes).</p>

                    <a href="/choice-awards/winners" class="btn btn-lg btn-action" style="margin-right: 4rem;">Winners List</a>
                    <a href="/sponsors" class="btn btn-lg btn-action">Become a sponsor</a>

                    <div class="row">
                        <div class="col-xs-4 volume-icon-container-outer">
                            <div class="volume-icon-container">
                                <img src="/images/icon-volume-medium.jpg" alt="">
                            </div>

                            <h4>Medium Volume Builders</h4>
                            <h5>(up to 125 homes)</h5>
                        </div>
                        <div class="col-xs-4 volume-icon-container-outer">
                            <div class="volume-icon-container">
                                <img src="/images/icon-volume-large.jpg" alt="">
                            </div>

                            <h4>Large Volume Builders</h4>
                            <h5>(126-275 homes)</h5>
                        </div>
                        <div class="col-xs-4 volume-icon-container-outer">
                            <div class="volume-icon-container">
                                <img src="/images/icon-volume-high.jpg" alt="">
                            </div>

                            <h4>High Volume Builders</h4>
                            <h5>(over 275 homes)</h5>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection