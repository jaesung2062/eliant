@extends('default')

@section('content')
    <div class="page page-contact">
        <header class="section-header bg-gradient contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 text-container">
                        <h4 class="text-uppercase">Contact Eliant</h4>
                        <h3>We're Ready to Answer the Call, Tweet, FB message, or Email.</h3>

                        <p>Ask us how we can help you and let’s build your success together. Our staff psychologist and account managers and are waiting to create your next questionnaires, interpret your customers’ feedback, and help create an Action Plan for delivering an extraordinary customer experience.</p>
                    </div>

                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/eliant-desktopCONTACT-PAGE-NEW_03.jpg" alt="" class="grid-image">
                            </div>
                            <div class="col-xs-6 bg-theme contact-information-container grid-image">



                                <h4><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp; Office Address:</h4>
                                <p>
                                    &nbsp;&nbsp;&nbsp;&nbsp;27141 Aliso Creek,<br>&nbsp;&nbsp;&nbsp;&nbsp;#290<br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;Aliso Viejo, CA 92656
                                </p>

                                <h4><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; Phone:</h4>
                                <p>
                                    <a href="tel:866-755-8199">&nbsp;&nbsp;&nbsp;&nbsp;+1 866-755-8199</a>
                                </p>

                                <h4><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; Email:</h4>
                                <p>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:answers@eliant.com">answers@eliant.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="section-contact bg-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <iframe
                                width="400"
                                height="400"
                                frameborder="0" style="border:0"
                                src="https://www.google.com/maps/embed/v1/place?key={{ config('services.google.maps_api_key') }}&q=27141+Aliso+Creek,+#290+Aliso+Viejo,+CA+92656" allowfullscreen>
                        </iframe>
                    </div>
                    <div class="col-xs-6">
                        @include('partials.alerts')
                        <br>
                        @include('partials.contact-form', ['buttonText' => 'Contact Eliant'])
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
