@extends('default')

@section('content')
    <div class="page page-home">
        <header class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-5 col-md-4 text-container">
                        <p>Answers to questions about your customers’ experience don’t mean squat unless they’re right. And they’re right only if they lead to greater sales success. For over 30 years,
                            <a href="/surveys" class="text-theme"><b>Eliant</b></a> has provided our clients with deep insights about their customers…the right answers to help them grow their business beyond expectations.
                        </p>
                    </div>
                    <div class="col-xs-7 col-md-8">
                        <h1>Providing Our Clients with the Right Answers.</h1>
                        {{--<h2 id="result">detecting…</h2>--}}
                        {{----}}
                        <div class="header-images-container">
                            <img src="/images/bubble-question.png" alt="bubble">
                            <img src="/images/bubble-answer.png" alt="bubble">
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="section-main">
            <!--BEGIN SURVEYS SECTION-->
            <section class="subsection-surveys contains-animations">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="row low-margin">
                                <div class="col-xs-6 col-xs-offset-6">
                                    <div class="image-top-left">
                                        <img src="/images/eliant-surveys-image-01.jpg" alt="" class="img-responsive grid-image image-survey-1">
                                    </div>
                                </div>
                            </div>

                            <div class="text-container text-left">
                                <h3>
                                    The Eliant Surveys.<br>
                                    Experience Better.
                                </h3>

                                <p>
                                    <a href="/surveys" class="text-theme"><b>The Eliant Surveys</b></a> are the heart of the company that started it all. Anyone can ask questions, but asking the RIGHT ones is what gives us an edge in understanding your customers’ experience. Getting evaluation results you can trust from the original homebuyer survey company is the best way to experience a world of better.
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <img src="/images/eliant-surveys-image-02.jpg" alt="" class="img-responsive grid-image image-survey-2">
                        </div>
                        <div class="col-xs-3">
                            <img src="/images/eliant-surveys-image-03.jpg" alt="" class="img-responsive grid-image image-survey-3">
                        </div>
                    </div>
                </div>
            </section>
            <!--END SURVEYS SECTION-->

            <!--BEGIN SOCIAL SECTION-->
            <section class="subsection-social contains-animations">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/images/images-home_01.jpg" alt="" class="img-responsive grid-image image-social-1">
                        </div>
                        <div class="col-xs-3">
                            <img src="/images/images-home_02.jpg" alt="" class="img-responsive grid-image image-social-2">
                        </div>
                        <div class="col-xs-6">
                            <div class="text-container">
                                <h3>
                                    Get Social.<br>
                                    Give Answers.
                                </h3>

                                <p>Turn your customers’ positive comments into praising tweets, posts and pics with
                                    <a href="/surveys#rateSOCIAL" class="text-theme"><b>rateSOCIAL</b></a>. The world relies on social media and so should you. Partner with Eliant and rateSOCIAL and we’ll help you build a social media marketing plan that will send shockwaves through the twitterverseblast through the blogosphere and deliver a whole new level of success for your business. No one knows more about your company than your customers.
                                </p>
                            </div>

                            <div class="row low-margin">
                                <div class="col-xs-6">
                                    <img src="/images/images-home_03.jpg" alt="" class="img-responsive grid-image image-social-3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--END SOCIAL SECTION-->

            <!--BEGIN CERTIFICATION SECTION-->
            <section class="subsection-certification contains-animations">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="text-container text-left">
                                <h3>
                                    Certify Your Team in Customer Awesomeness.
                                </h3>

                                <p>The
                                    <a href="/services#eliant-certification" class="text-theme"><b>Eliant Certification Program</b></a> was designed to motivate your employees to become customer service ninjas. It’s an incredibly effective way to evaluate, motivate, and recognize your team’s performance. Eliant Certification allows you to hold your team members accountable for consistently delivering an incredible “OMG” customer experience.
                                </p>
                            </div>

                            <div class="row low-margin">
                                <div class="col-xs-6 col-xs-offset-6">
                                    <img src="/images/certification-image-01.jpg" alt="" class="img-responsive grid-image image-certification-1">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <img src="/images/certification-image-02.jpg" alt="" class="img-responsive grid-image image-certification-2">
                        </div>
                        <div class="col-xs-3">
                            <img src="/images/certification-image-03.jpg" alt="" class="img-responsive grid-image image-certification-3">
                        </div>
                    </div>
                </div>
            </section>
            <!--END CERTIFICATION SECTION-->

            <!--BEGIN HOMEBUYER CHOICE SECTION-->
            <section class="subsection-choice contains-animations">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/images/eliant-home-page-awards_06.png" alt="" class="img-responsive grid-image homebuyer-choice-2">
                        </div>
                        <div class="col-xs-3">
                            <img src="/images/eliant-home-page-awards_03.jpg" alt="" class="img-responsive grid-image homebuyer-choice-3">
                        </div>
                        <div class="col-xs-6">
                            <div class="text-container">
                                <h3>See if Homebuyers Chose YOU.</h3>

                                <p>One of the most anticipated events in the home building industry is the annual
                                    <a href="/choice-awards" class="text-theme"><b>Home Buyers Choice Awards</b></a>. This is a gala award ceremony that takes place in a magnificent venue near Eliant’s national headquarters.
                                </p>
                            </div>

                            <div class="row low-margin">
                                <div class="col-xs-6">
                                    <img src="/images/eliant-home-page-awards_09.png" alt="" class="img-responsive grid-image homebuyer-choice-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--END HOMEBUYER CHOICE SECTION-->
        </section>
    </div>
    </div>

@endsection
