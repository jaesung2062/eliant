@extends('default')
{{--Test--}}
@section('content')
    <div class="page page-about">
        <section class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            <h4 class="subheading">About Eliant</h4>
                            <h3>We Were the First to be Right.</h3>

                            <p>We are the ORIGINAL homebuyer survey company for the new home industry. Eliant has been conducting intensive customer research and evaluations nationwide for over 34 years and has conducted over 2.5 million consumer surveys!</p>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/about-page-desktop-Eliant-Sept-11-2017_03.jpg" alt="" class="img-responsive grid-image image-about-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/about-page-desktop-Eliant-Sept-11-2017_06.jpg" alt="" class="img-responsive grid-image image-about-2">
                    </div>
                </div>
            </div>
        </section>



        <section class="section-business">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 title-container text-center">
                        <h1>When you do business with Eliant it just feels right.</h1>

                        <br>
                        <p>Our Eliant team assists clients in many industries. Here’s a list of some of our clients that have benefited from our game-changing customer evaluations. Why not add yours to it?</p>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    @include('partials.alerts')
                    <br>
                    @include('partials.contact-form', ['buttonText' => 'Contact Eliant'])
                </div>
            </div>
        </div>


    </div>
@endsection
