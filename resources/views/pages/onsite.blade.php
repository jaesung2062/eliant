@extends('default')
{{--Test--}}
@section('content')
    <div class="page page-about">
        <section class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            {{--<h4 class="subheading">About Eliant</h4>--}}
                            <h3 style="color: #eaa521;">Got Employees?<br>We've got all the answers.</h3>

                            <p>A major division of Eliant is dedicated to the real estate industry,
                                but we evaluate customer perceptions for many other industries
                                 as well. No matter what field you’re in, we can collect employee
                                and customer evaluations, which provide the right answers to help
                                your business grow in ways you never thought possible. The Eliant
                                Surveys offer products to fit any industry.</p>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/onsite_images/top-left-onsite.jpg" alt="" class="img-responsive grid-image image-about-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/onsite_images/top-right-onsite.jpg" alt="" class="img-responsive grid-image image-onsite-2">
                    </div>
                </div>
            </div>
        </section>



        <section class="section-answers contains-animations" style="margin-bottom: 50px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 image-column">
                        <img src="/images/onsite_images/bottom-left.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/onsite_images/bottom-right.jpg" alt="" class="grid-image top-down-ninety" style="margin-top: 50%;">
                    </div>
                    <div class="col-xs-5 col-xs-offset-1 copy-column text-container on-right-side" style="    margin-left: 75px;">
                        <h1 class=" top-down-thirty" style="color: #eaa521;">Eliant Onsite-Leaders:
                             builders learning how to
                            lead the key behaviors
                            that drive referrals.</h1>

                        <p>Sales, Lender, Design, Construction, and Customer Care leaders manage employees who deal with customers. Learn what key  employee behaviors drive customer referrals most. Focus on the vital few items and drive real change.
                            <br><br>Eliant uses company, nation wide, and best builder comparisons to allow a unique look into what could be improved on. Allow eliant to share its “secret sauce” for improving company customer satisfaction and referral rate results. Contact us below for more information.</p>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    @include('partials.alerts')
                    <br>
                    @include('partials.onsite', ['buttonText' => 'Contact Eliant'])
                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
