@extends('default')

@section('content')
    <div class="page page-services">
        <header class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            <h4 class="text-uppercase">Eliant Services</h4>

                            <h3>The Answer to Your Success is More Than Customer Surveys.</h3>

                            <p>Eliant is all about providing continuous improvement insights to our clients. Our services and real-time reports offer a deeper and clearer look into what your customers are thinking. Now you can have a better understanding of how to make their experience better and how to get that edge on your competition.</p>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <img src="/images/servvices-page_03.jpg" alt="" class="img-responsive grid-image services-header-image-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/servvices-page_06.jpg" alt="" class="img-responsive grid-image services-header-image-2">
                    </div>
                </div>
            </div>
        </header>

        <section class="section-ceo contains-animations">
            <a id="speaking-engagements"></a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <img src="/images/ceo.jpg" alt="" class="img-responsive grid-image eliant-ceo-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/ceo-friend.jpg" alt="" class="img-responsive grid-image eliant-ceo-2">
                    </div>
                    <div class="col-xs-6">

                        <div class="copy-container on-right-side">
                            <h1>Speaking Engagements. Not the Same Old Show</h1>

                            <p>As Eliant’s founder, CEO, and psychologist, Bob Mirman’s international presentations focus on the psychological and commercial aspects of the homebuyers’ customer experience. Bob provides humor and invaluable insight into interpreting survey scores and rankings and is renowned as a keynote speaker at corporate and building-industry events such as PCBC, IBS, ECBC, and the BIS show.</p>

                            <p>Jex Manwaring, Eliant’s president since 2014, has served as a leader of a large, multi-divisional builder and conducts masterful workshops for leaders and customer-facing employees. He has used his invaluable hands-on experience and unique, effective leadership techniques to implement powerful changes to some of the biggest names in the industry. <a href="/contact"><b>Inquire within</b></a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-training contains-animations">
            <a id="training-programs"></a>
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">

                        <div class="copy-container on-left-side">
                            <h1 class="text-left">Get Training & Deliver the "OMG" Factor</h1>

                            <p class="text-left">The first step is upgrading the behavior of everyone on your team so they can consistently “delight” the customer and exceed expectations, all while holding them accountable for delivering the “OMG” factor. Our step-by-step ‘Customer Experience Blueprint’ makes this easy. Most training programs just impart information, our training is designed to improve and transform your teams’ behaviors…and your ratings. <a href="/contact"><b>Get started today</b></a>.</p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/servvices-page_17.jpg" alt="" class="img-responsive grid-image get-training-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/servvices-page_20.jpg" alt="" class="img-responsive grid-image get-training-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-eliant-certification contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <img src="/images/servvices-page_26.jpg" alt="" class="img-responsive grid-image competitive-edge-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/servvices-page_24.jpg" alt="" class="img-responsive grid-image competitive-edge-2">
                    </div>
                    <div class="col-xs-6">
                        <a id="eliant-certification"></a>
                        <div class="copy-container on-right-side">
                            <h1>Eliant Certification. Build Your Competitive Edge.</h1>

                            <p>The Eliant Certification Program motivates and rewards your employees for upping their game and improving their customer ratings. But what really makes this program unique are the psychological techniques developed by our clinical psychologist and CEO, Bob Mirman. These techniques take a deeper, more methodical approach to asking the right questions in order to deliver the right answers. <br> <a href="/contact"><b>Contact us</b></a> for more information.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
