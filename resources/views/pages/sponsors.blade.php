@extends('default')

@section('content')
    <div class="page page-sponsors">
        <section class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container on-left-side">
                            <h4 class="text-uppercase">Sponsorships</h4>
                            <h1>Eliant Sponsorship Opportunities</h1>

                            <p>
                                Eliant is all about providing continuous improvements insights to our clients. Our
                                services and real-time report data offer a deeper and clearer look into what your
                                customers are thinking. Now you can have a better understanding of how to make their
                                experience better and how to get that edge on your competition.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/hands-up-2.jpg" alt="" class="grid-image hands-up-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/hands-up-1.jpg" alt="" class="grid-image hands-up-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{--<section class="section-gifts contains-animations">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="row low-margin">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/presents-1.jpg" alt="" class="grid-image presents-1">--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<img src="/images/presents-2.jpg" alt="" class="grid-image presents-2">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<div class="copy-container text-container on-right-side">--}}
                            {{--<h1>Gift Sponsor: $450</h1>--}}
                            {{--<h3>2 sponsors available</h3>--}}

                            {{--<ul>--}}
                                {{--<li>Logo listed under Sponsor Level on HBC Awards page</li>--}}
                                {{--<li>Logo under Sponsor Level on all Eliant generated collateral materials</li>--}}
                                {{--<li>Logo listed under Sponsor Level in event program</li>--}}
                                {{--<li>GIFT SPONSOR ONLY: choice of either donating your premium logo</li>--}}
                                {{--<li>item, or sponsoring the iPad (or similar tech item) raffle</li>--}}
                            {{--</ul>--}}

                            {{--<p><a href="" class="link-sponsor-modal" data-type="gift">Become a Gift Sponsor</a></p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}

        <section class="section-silver contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="copy-container text-container on-left-side">
                            <h1>Silver Sponsor: $1,450</h1>
                            <h3>2 sponsors available</h3>
                            {{--<p>All the features of the GIFT SPONSOR level PLUS...</p>--}}

                            <ul>
                                <li>Company name and logo listed in introduction and closing PowerPoint loop</li>
                                <li>Logo listed under Sponsor Level on Homebuyers’ Choice Awards (HBC) page</li>
                                <li>Logo under Sponsor Level on all Eliant generated collateral materials</li>
                                <li>Logo listed under Sponsor Level in event program</li>
                            </ul>

                            <p><a href="" class="link-sponsor-modal" data-type="silver">Become a Silver Sponsor</a></p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/silver-2.jpg" alt="" class="grid-image silver-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/silver-1.jpg" alt="" class="grid-image silver-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-gold contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/gold-2.jpg" alt="" class="grid-image gold-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/gold-1.jpg" alt="" class="grid-image gold-2">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="copy-container text-container on-right-side">
                            <h1>Gold Sponsor: $2,450</h1>
                            <h3>4 sponsors available</h3>
                            <p>All the features of the SILVER SPONSOR level PLUS...</p>

                            <ul>
                                <li>Announcement of sponsorship by main presenter, including brief highlighted statement about your firm</li>
                                <li>2 event tickets, savings of $170</li>
                                <li>Logo listed under Sponsor Level on signage at event</li>
                                <li>Full slide logo and company name in introduction and closing PowerPoint loop</li>
                                <li>Listed in press release</li>
                                <li>Logo listed under Sponsor Level on HBC Awards page, under Sponsor Level on all Eliant generated collateral materials, listed under Sponsor Level in event program</li>
                            </ul>

                            <p><a href="" class="link-sponsor-modal" data-type="gold">Become a Gold Sponsor</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-platinum contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="copy-container text-container on-left-side">
                            <h1>Platinum Sponsor: $2,950</h1>
                            <h3>3 sponsors available</h3>
                            <p>All the features of the GOLD SPONSOR level PLUS...</p>

                            <ul>
                                <li>Opportunity to announce winner on-stage and present award:</li>
                                <ul>
                                    <li>
                                        Purchase experience - Design experience - Construction experience
                                    </li>
                                </ul>
                                <li>4 event tickets, savings of $340</li>
                                <li>Opportunity to provide collateral material to be placed at table setting, max quarter page size</li>
                                <li>Announcement of sponsorship by Eliant’s main presenter, including a brief highlighted statement about your firm</li>
                                <li>Included in press release as a primary sponsor</li>
                                <li>One slide in HBC Awards PowerPoint presentation, to be shown during presentation</li>
                                <li>Full slide logo and company name in introduction and closing PowerPoint loop</li>
                                <li>Logo listed: under Sponsor Level on HBC Awards page, listed under Sponsor Level on signage at event, Logo under Sponsor Level on all Eliant generated collateral materials, listed under Sponsor Level in event program</li>
                            </ul>

                            <p><a href="" class="link-sponsor-modal" data-type="platinum">Become a Platinum Sponsor</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/platinum-2.jpg" alt="" class="grid-image platinum-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/platinum-1.jpg" alt="" class="grid-image platinum-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-experience contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/victory-1.jpg" alt="" class="grid-image victory-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/victory-2.jpg" alt="" class="grid-image victory-2">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="copy-container text-container on-right-side">
                            <h1>Customer Experience Leader of the Year Sponsor: $4,350 (SOLD)</h1>
                            <h3>0 sponsors available</h3>
                            <p>Purchased by First American Title</p>

                            <ul>
                                <li>Opportunity to announce winner on-stage and present the award</li>
                                <li>4 event tickets, savings of $340</li>
                                <li>Opportunity to provide collateral material to be placed at table setting, half page size</li>
                                <li>Announcement of sponsorship by Eliant’s main presenter, including brief highlighted statement about your firm</li>
                                <li>Spotlight in press release as Customer Experience Leader of the Year Sponsor and listed as a sponsor</li>
                                <li>Two slides in HBC Awards PowerPoint presentation, to be shown during presentation</li>
                                <li>Full slide logo and company name in introduction and closing PowerPoint loop</li>
                                <li>Logo listed: under Sponsor Level on HBC Awards page, listed under Sponsor Level on signage at event, Logo under Sponsor Level on all Eliant generated collateral materials, listed under Sponsor Level in event program</li>
                            </ul>

                            <p><a href="" class="link-sponsor-modal" data-type="leader">Become a Leader Sponsor</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-premium contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="copy-container text-container on-left-side">
                            <h1>Premier Sponsor: $14,000 (SOLD)</h1>
                            <p>Purchased by loanDepot</p>

                            <ul>
                                <li>Opportunity to provide collateral material to be set at each place setting (full page size)</li>
                                <li>10 event tickets, savings of $850</li>
                                <li>Introduction by Eliant CEO and opportunity to present a 5-minute speech prior to awards presentation</li>
                                <li>Opportunity to show a 2-minute video to kick off the awards presentation</li>
                                <li>Announcement of the Eliant Award – single division & multiple division winners</li>
                                <li>Prominent signage at the event</li>
                                <li>Co-branding on “step and repeat” photo wall</li>
                                <li>Spotlight in press release as Premier Sponsor along with being listed as sponsor</li>
                                <li>Three slides in introduction and closing PowerPoint loop</li>
                                <li>Three slides throughout the HBC Awards PowerPoint presentation</li>
                                <li>Logo listed: under Sponsor Level on HBC Awards page, listed under Sponsor Level on signage at event, Logo under Sponsor Level on all Eliant generated collateral materials, listed under Sponsor Level in event program</li>
                            </ul>

                            <p><a href="" class="link-sponsor-modal" data-type="premier">Become a Premier Sponsor</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row low-margin">
                            <div class="col-xs-6">
                                <img src="/images/spotlights-2.jpg" alt="" class="grid-image spotlights-1">
                            </div>
                            <div class="col-xs-6">
                                <img src="/images/spotlights-1.jpg" alt="" class="grid-image spotlights-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="modal fade" tabindex="-1" role="dialog" id="sponsor-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="" class="bg-gradient" id="sponsor-form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Become a Sponsor</h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger hidden" id="sponsor-form-errors">
                                <ul></ul>
                            </div>

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="hidden" aria-hidden="true"></label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="* Name">
                            </div>

                            <div class="form-group">
                                <label for="email" class="hidden" aria-hidden="true"></label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="* Email">
                            </div>

                            <div class="form-group">
                                <label for="phone_number" class="hidden" aria-hidden="true"></label>
                                <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number">
                            </div>

                            <div class="form-group">
                                <label for="type" class="hidden" aria-hidden="true"></label>
                                <select name="type" id="type" class="form-control">
                                    <option value="gift">Gift</option>
                                    <option value="silver">Silver</option>
                                    <option value="gold">Gold</option>
                                    <option value="platinum">Platinum</option>
                                    <option value="leader">Leader</option>
                                    <option value="premier">Premier</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="message" class="hidden" aria-hidden="true"></label>
                                <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-action">Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
@endsection
