@extends('default')

@section('content')
    <div class="page page-surveys">
        <header class="section-header contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            <h4>Eliant Products</h4>

                            <h3>The Eliant Surveys. <br> Experience Better.</h3>

                            <p>The Eliant Surveys provide an accurate, in-depth and timely analysis of customers’ evaluations of their overall experience with your company and specific behaviors of your team members. So, you’ll see the areas where you kick butt and where you’re getting your butt kicked.</p>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <img src="/images/images-surveys_03.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/images-surveys_06.jpg" alt="" class="grid-image top-pushed-down">
                    </div>
                </div>
            </div>
        </header>

        <section class="section-answers contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_10.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_14.jpg" alt="" class="grid-image top-down-ninety">
                    </div>
                    <div class="col-xs-6 copy-column text-container on-right-side">
                        <h1 class=" top-down-thirty">Got customers? We’ve got all the answers.</h1>

                        <p>A major division of Eliant is dedicated to the real estate industry, but we evaluate customer perceptions for many other industries as well. No matter what field you’re in, we can collect customer evaluations which provide the right answers to help your business grow in ways you never thought possible. The Eliant Surveys offer products and methodologies to fit any industry.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-experience contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 copy-column text-container on-left-side">
                        <h1 style="color: #F8931D">rateEXPERIENCE ™ - surveys for any industry.</h1>
                        <a name="rateEXPERIENCE"></a>

                        <p>No matter what your business is, if you have customers we have a
                            customer experience review process which can be customized for your specific industry. Our evaluations provide valuable data to give you a deeper insight into what prospective customers want and how well you’re meeting their needs. Keep them delighted. Keep them coming back. Keep getting referrals. <a href="/contact"><b>Contact us</b></a> to see how your business can benefit from our unique evaluation process.</p>

                        <img src="/images/mobile/rateExperience-logo.jpg" alt="">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_18.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_20.jpg" alt="" class="grid-image top-down-ninety">
                    </div>
                </div>
            </div>
        </section>

        <section class="section-home contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_23.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_27.jpg" alt="" class="grid-image top-down-seventhree">
                    </div>
                    <div class="col-xs-6 copy-column text-container on-right-side">
                        <h1 style="color: #0FC4CF" class="top-down-eighteen">rateHOME ™ - the #1 evaluation program for new home builders.</h1>
                        <a name="rateHOME"></a>

                        <p>With over 2 million surveys to date, rateHOME (previously the Eliant Benchmark Survey Series) is the premier evaluation program for new home builders nationwide. Our 2016 average survey response rate was over 70%! Now that’s something to write home about. Want more info about rateHOME?  <a href="/contact"><b>Call or email us today.</b></a></p>

                        <img src="/images/mobile/ratehome-logo.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="section-lender contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 copy-column text-container on-left-side">
                        <h1 style="color: #EB3124">rateLENDER ™ - the perfect answer for lenders.</h1>
                        <a name="rateLENDER"></a>

                        <p>Finding the money to buy a new home is probably the hardest step for new home buyers. With rateLENDER, you’ll be able to hold your loan officers and processors accountable for delivering an extraordinary mortgage experience. Want answers?  <a href="/contact"><b>Ask us</b></a>.</p>

                        <img src="/images/mobile/rateLender-logo.jpg" alt="">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_31.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_32.jpg" alt="" class="grid-image top-down-ninetyfour">
                    </div>
                </div>
            </div>
        </section>

        <section class="section-trades contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 image-column">
                        <img src="/images/rateTrade.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-6 copy-column text-container on-right-side">
                        <h1 style="color: #65D00E">rateTRADES ™ - builders review of sub-contractors.</h1>
                        <a name="rateTRADES"></a>

                        <p>Superintendents, customer service representatives, and purchasing agents evaluate the work of each trade (sub-contractor). Eliant uses this input to rank your trades against other similar trades in the market. Eliant also offers the TradeStar program that allows builders to easily share their ratings with their trade partners. Now, subcontractors can view builder feedback and then formulate and execute specific action plans. <a href="/ratetrades"><b>Contact us for more information.</b></a>
                        </p>

                        <img src="/images/mobile/rateTrades-logo.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="section-builder contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 copy-column text-container on-left-side">
                        <h1 style="color: #B59F6E">rateBUILDER ™ - exclusive to Eliant for new home builders.</h1>
                        <a name="rateBUILDER"></a>

                        <p>Sub-contractors can now rate new home/condo/apartment builders based on their experience. Builders can then see their own ratings and how they match up against other builders in the area. Want to be reviewed better than your competitors? <a href="/contact"><b>Ask us how</b></a>.</p>

                        <img src="/images/mobile/rateBuilder-logo.jpg" alt="">
                    </div>
                    <div class="col-xs-6 image-column">
                        <img src="/images/ratebuilder.jpg" alt="" class="grid-image">
                    </div>
                </div>
            </div>
        </section>

        <section class="section-social contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_37.jpg" alt="" class="grid-image top-down-ninetyfour">
                    </div>
                    <div class="col-xs-3 image-column">
                        <img src="/images/images-surveys_35.jpg" alt="" class="grid-image">
                    </div>
                    <div class="col-xs-6 copy-column text-container on-right-side">
                        <h1 style="color: #A95DFD">rateSOCIAL - it's a big deal.</h1>
                        <a name="rateSOCIAL"></a>

                        <p>Don’t know if you’ve heard, but social media is kind of a big deal these days. So, leverage it with rateSOCIAL. Allow your prospective buyers to see what your current customers have to say about you! Now all of your customer feedback posts can be linked to your Facebook and Twitter accounts to get real-time evaluation data, increase website sales and improve Google search results. <a href="/contact"><b>Contact us via Facebook for the social experience, or just call</b></a>.</p>

                        <img src="/images/mobile/rateSocial-logo.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>


        {{--TEST OF RATEAPARTMENTS--}}

        <section class="section-builder contains-animations">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 copy-column text-container on-left-side">
                        <h1 style="color: #B59F6E">rateAPARTMENTS ™ - because apartments need some love too.</h1>
                        <a name="rateAPARTMENTS"></a>

                        <p>Most of the population lives in apartment buildings, which makes them goldmines of tenant feedback. With rateApartment, you’ll be able to mine that precious data and gain insight into how tenants truly feel about many issues like building quality, amenities, leasing reps, move-in process, and the overall apartment living experience. As an apartment owner, you’ll be able to see online or emailed reports at intervals of your choosing regarding what works and what needs work. All this valuable information will help you improve the processes and quality of your apartment community so you can attract new tenants, get current tenants to renew their leases, and secure more referrals. Contact us today to start digging into your goldmine of tenant survey data. <a href="/contact"><b>We’ve got the answers to help you hit pay dirt.</b></a></p>

                        <img src="/images/mobile/rateBuilder-logo.jpg" alt="">
                    </div>
                    <div class="col-xs-6 image-column">
                        <img src="/images/rate_apts.png" alt="" class="grid-image">
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
