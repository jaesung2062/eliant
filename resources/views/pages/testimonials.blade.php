@extends('default')

@section('content')
    <div class="page page-testimonials">
        <header class="section-header contains-animations bg-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="text-container text-left">
                            <h4 class="text-uppercase">Testimonials</h4>
                            <h3>See Our Clients' Answers Before You Ask For Ours.</h3>

                            <p>We’ve helped many clients just like you, with questions just like yours. Listen to what real people are saying about our answers, and when you’re ready we’ll get some for <a href="#">you, too</a>.</p>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <img src="/images/eliant-desktopTESTIMONIALS-PAGE-NEW-_03.jpg" alt="" class="img-responsive grid-image image-testimonials-1">
                    </div>
                    <div class="col-xs-3">
                        <img src="/images/eliant-desktopTESTIMONIALS-PAGE-NEW-_06.jpg" alt="" class="img-responsive grid-image image-testimonials-2">
                    </div>
                </div>
            </div>
        </header>

        <section class="section-testimonials">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            @foreach($testimonials->chunk($testimonials->count() / 3 + 1) as $chunk)
                                <div class="col-xs-4 testimonials-column">
                                    @foreach($chunk as $testimonial)
                                        <div class="testimonial">
                                            <img src="{{ $testimonial->getPersonImagePath() }}" alt="" class="img-responsive">

                                            <blockquote>"{{ $testimonial->quote }}"</blockquote>

                                            <p class="quote-info">
                                                {{ $testimonial->person }} <br>
                                                {{ $testimonial->company }}
                                            </p>

                                            @if($testimonial->getCompanyImagePath())
                                                <img src="{{ $testimonial->getCompanyImagePath() }}" alt="" class="img-responsive">
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="row">
                            <div class="contact-form-container">
                                <div class="clearfix"></div>
                                <h3 class="text-uppercase text-center">Contact Us</h3>

                                <p class="text-center">
                                    <a href="tel:866-755-8199">(866) 755-8199</a>
                                </p>

                                @include('partials.contact-form')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
