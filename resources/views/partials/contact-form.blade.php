 <form action="/contact/send" method="post" class="contact-form">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">*Name</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
    </div>

    <div class="form-group">
        <label for="email">*Email</label>
        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
    </div>

    <div class="form-group">
        <label for="number">*Phone Number</label>
        <input type="text" class="form-control" id="number" name="number" value="{{ old('number') }}" required>
    </div>

    <div class="form-group">
        <label for="subject">Subject</label>
        <input type="text" class="form-control" id="subject" name="subject" value="{{ old('subject') }}">
    </div>

    <div class="form-group">
        <label for="body">Message</label>
        <textarea class="form-control" id="body" name="body" cols="30" rows="10">{{ old('body') }}</textarea>
    </div>

    <button class="btn btn-action btn-lg text-uppercase center-block">Submit</button>
</form>
