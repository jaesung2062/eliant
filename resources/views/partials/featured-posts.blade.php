<div class="featured-posts">
    <div class="clearfix"></div>

    <h4>Featured Posts</h4>

    @foreach($posts->slice(0, 5) as $post)

        <a href="/blog/{{ $post->ID }}/{{ str_slug($post->title) }}" class="featured-post">
            {{ $post->title }}
            <i class="fa fa-caret-right"></i>
        </a>

    @endforeach
</div>