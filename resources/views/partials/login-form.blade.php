<form action="{{ $_action }}" method="post" class="panel-body">
    <div class="form-group form-group-lg">
        <label for="username-rate-trades" class="hidden">Username</label>
        <input id="username-rate-trades" type="text" class="form-control" name="ctl00$ContentPlaceHolder1$email" placeholder="* USER NAME" value="{{ old('username') }}">
    </div>

    <div class="form-group form-group-lg">
        <label for="password-rate-trades" class="hidden">Password</label>
        <input id="password-rate-trades" type="password" class="form-control" name="ctl00$ContentPlaceHolder1$password" placeholder="* PASSWORD">
    </div>

    <br>

    <button class="btn btn-lg">Login</button>

    <br><br>

    <a href="{{ $forgot }}" class="forgot-password">Forgot Password?</a>
</form>
