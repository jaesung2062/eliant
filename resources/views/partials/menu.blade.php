<nav id="menu">
    <div class="container-fluid">
        <div class="row">
            <a href="#" class="menu-close-button">close &times;</a>
            <div class="col-xs-4">
                <a href="/about" class="menu-link"><span>About Eliant</span></a>
            </div>
            <div class="col-xs-4">
                <a href="/testimonials" class="menu-link"><span>Testimonials</span></a>
            </div>
            <div class="col-xs-4">
                <a href="/blog" class="menu-link"><span>The Eliant Blog</span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <a href="/surveys" class="menu-link has-children"><span>Surveys</span></a>

                <ul class="child-links">
                    <li><a href="/surveys#rateEXPERIENCE">rateEXPERIENCE</a></li>
                    <li><a href="/surveys#rateHOME">rateHOME</a></li>
                    <li><a href="/surveys#rateLENDER">rateLENDER</a></li>
                    <li><a href="/surveys#rateTRADES">rateTRADES</a></li>
                    <li><a href="/surveys#rateBUILDER">rateBUILDER</a></li>
                    <li><a href="/surveys#rateSOCIAL">rateSOCIAL</a></li>
                    <li><a href="/surveys#rateAPARTMENTS">rateAPARTMENTS</a></li>
                </ul>
            </div>
            <div class="col-xs-4">
                <a href="/services" class="menu-link has-children"><span>Services</span></a>

                <ul class="child-links">
                    <li><a href="/services#speaking-engagements">Speaking Engagement</a></li>
                    <li><a href="/services#training-programs">Training Programs</a></li>
                    <li><a href="/services#eliant-certification">Eliant Certification</a></li>
                </ul>
            </div>
            <div class="col-xs-4">
                <a href="/faq" class="menu-link"><span>FAQ</span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <a href="/choice-awards" class="menu-link"><span>Home Buyers Choice Award</span></a>
            </div>
            <div class="col-xs-4">
                <a href="/sponsors" class="menu-link"><span>Sponsors</span></a>
            </div>
            <div class="col-xs-4">
                <a href="/contact" class="menu-link"><span>Contact Us</span></a>
            </div>
        </div>
    </div>
</nav>