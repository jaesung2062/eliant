<div class="modal modal-winners-list fade" data-modal-list="{{ $list->id }}">
    <div class="modal-dialog modal-lg winners-list-modal" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>

                <h3 class="text-center">{{ $list->name }}</h3>

                @if($list->type == 'categorized')
                    @foreach($list->winners as $category => $winners)
                        <ul class="winners-list categorized-winners-list">
                            <li class="list-category bg-theme">{{ $category }}</li>

                            @foreach($winners as $i => $winner)
                                <li>
                                    <span class="text-theme">
                                        @if($winner['place'] === 1)
                                            1st place:
                                        @elseif($winner['place'] === 2)
                                            2nd place:
                                        @elseif($winner['place'] === 3)
                                            3rd place:
                                        @else
                                            Honorable Mention:
                                        @endif

                                        {{--This checks whether there are any ties and outputs (TIE) if there is--}}
                                        @if($winner['place'] != 'h' && count(array_filter($winners, function ($item) use ($winner) { return $winner['place'] == $item['place']; })) > 1)
                                            (TIE)
                                        @endif
                                    </span>

                                    {{ $winner['name'] }} - {{ $winner['location'] }}
                                </li>
                            @endforeach

                            <li class="spacer"></li>
                        </ul>
                    @endforeach
                @else
                    <ul class="winners-list">
                        <li class="bg-theme text-center">{{ $list->description }}</li>
                        @foreach($list->winners as $i => $winner)
                            <li>
                                @if( ! $list->is_alphabetical)
                                    <span class="text-theme">
                                        @if($i === 0)
                                            1st place:
                                        @elseif($i === 1)
                                            2nd place:
                                        @elseif($i === 2)
                                            3rd place:
                                        @else
                                            Honorable Mention:
                                        @endif
                                    </span>
                                @endif

                                {{ $winner['name'] }} - {{ $winner['location'] }}
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>
