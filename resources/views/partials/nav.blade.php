<div itemscope itemtype="http://schema.org/LocalBusiness" id="wrapper">
<nav id="navbar" class="navbar-fixed-top">
    <div class="container">
        <div class="row">
            {{--<div class="login-dropdown rate-trades-login-dropdown">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<img src="/images/mobile/rateTrades-logo.jpg" alt="eliant rate trades logo" class="rate-trades-logo">--}}
                    {{--</div>--}}
                    {{--@include('partials.login-form', ['_action' => 'http://ratetrades.eliant.com/Login.aspx', 'forgot' => 'http://ratetrades.eliant.com/Forgot.aspx'])--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="login-dropdown eliant-portal-login-dropdown">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<img src="/images/the-eliant-reportal-logo.jpg" alt="eliant reportal logo" class="rate-reportal-logo">--}}
                    {{--</div>--}}
                    {{--@include('partials.login-form', ['_action' => 'https://transport.eliant.com/', 'forgot' => 'https://transport.eliant.com/PasswordRecovery.aspx'])--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="col-xs-4">
                <div class="nav-brand-container-outer">
                    <div class="nav-brand-container text-center">
                        <a href="/" class="nav-brand">
                            <img itemprop="image" src="/images/eliant-logo.png" alt="Eliant Logo">
                        </a>
                    </div>

                    <a href="#" role="button" class="menu-dropdown-button text-center">
                        <img src="/images/mobile/menu-icon.png" alt="" class="menu-icon">
                        MENU
                    </a>
                </div>
            </div>

            <div class="col-xs-8 login-buttons-container">
                <a href="http://ratetrades.eliant.com/Login.aspx" class="rate-trades-button">
                    <img src="/images/padlock.jpg" alt="">
                    rateTRADES login
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="https://transport.eliant.com/" class="eliant-portal-button">
                    <img src="/images/padlock.jpg" alt="">
                    Eliant Analyt!cs login
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="tel:866-755-8199" class="phone-button">
                    (866) 755-8199
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/contact">

                    <i class="fa fa-envelope"></i>contact
                </a>
            </div>
        </div>
    </div>
</nav>
