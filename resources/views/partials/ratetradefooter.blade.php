<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <p itemprop="telephone">(949) 753-1077 ext. 21</p>
                <p><a href="mailto:ratetrades@eliant.com"><span itemprop="email">ratetrades@eliant.com</span></a></p>
            </div>
            <div class="col-sm-4 text-center">
                <p class="footer-links">
                    <a href="https://www.linkedin.com/company/68026?trk=tyah&trkInfo=tas%3Aeliant%2C%20" target="_blank">
                        <i class="fa fa-linkedin fa-3x"></i>
                    </a>
                    <a href="https://www.facebook.com/Eliantinc" target="_blank">
                        <i class="fa fa-facebook fa-3x"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCFRCVl4zMevDMwZfG0ge8Sw" target="_blank">
                        <i class="fa fa-youtube-play fa-3x"></i>
                    </a>
                </p>
            </div>
            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="col-sm-4 text-right">
                <p>
                    <span itemprop="streetAddress">27141 Aliso Creek, #290</span><br>
                    <span itemprop="addressLocality">Aliso Viejo</span>,
                    <span itemprop="addressRegion">CA</span>
                    <span itemprop="postalCode">92656</span>
                </p>
                <meta itemprop="addressCountry" content="US"></div>
        </div>
    </div>
</footer>

<meta itemprop="name" content="Eliant">
<span itemprop="openingHoursSpecification" itemscope itemtype="http://schema.org/OpeningHoursSpecification">
    <span itemprop="dayOfWeek" itemscope itemtype="http://schema.org/DayOfWeek">
        <meta itemprop="name" content="Mo-Su 24:00"></span></span>