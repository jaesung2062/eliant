<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

function registerRoutes() {
    Auth::routes();

    Route::get('/', 'PagesController@home');
    Route::get('/about', 'PagesController@about');
    Route::get('/landingOne', 'PagesController@landingOne');
    Route::get('/landingTwo', 'PagesController@landingTwo');
    Route::get('/landingThree', 'PagesController@landingThree');
    Route::get('/surveys', 'PagesController@surveys');
    Route::get('/services', 'PagesController@services');
    Route::get('/testimonials', 'PagesController@testimonials');
    Route::get('/choice-awards', 'PagesController@choiceAwards');
    Route::get('/choice-awards/winners', 'PagesController@choiceAwardsWinners');
    Route::get('/faq', 'PagesController@faq');

    Route::get('/onsite', 'PagesController@onsite');
    Route::post('/onsite', 'ContactController@onsite');
    Route::get('/ratetrades', 'PagesController@ratetrades');
    Route::post('/ratetrades', 'ContactController@ratetrades');


    Route::get('/yearendsurvey', 'PagesController@cem');
    Route::get('/midyearsurvey', 'PagesController@cem');
    Route::get('/moveinsurvey', 'PagesController@cem');
    Route::get('/hbc', 'PagesController@hbc');

    Route::get('/contact', 'ContactController@show');
    Route::post('/contact/send', 'ContactController@submit');
    Route::post('/contact', 'ContactController@sub');

    Route::get('/sponsors', 'PagesController@sponsors');
    Route::post('/sponsors', 'PagesController@postSponsors');

    Route::get('/blog/{id}/{slug}', 'BlogController@show');
    Route::resource('blog', 'BlogController');
    Route::post('/blog', 'ContactController@subscribe');

    Route::post('avatars', function () {

        request()->attachPersonalImagefile('avatar'); //->store('avatars')

        return back();
    });



}

registerRoutes();

Route::group(['subdomain' => 'm.'.config('app.domain')], function () {
    registerRoutes();
});

// admin routes
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'admin'
], function () {
    Route::get('/', 'AdminController@dashboard');
    Route::post('/testimonials/reorder', 'TestimonialsController@reorder');
    Route::resource('testimonials', 'TestimonialsController');
    Route::resource('awards', 'WinnersListController');


    Route::post('avatars', function () {

        request()->attachPersonalImagefile('avatar'); //->store('avatars')

        return back();
    });

});

Auth::routes();

Route::get('/home', 'HomeController@index');
