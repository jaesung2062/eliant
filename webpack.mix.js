const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/mobile.js', 'public/js/mobile.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/mobile/app.scss', 'public/css/mobile.css')
    .copyDirectory('resources/assets/images', 'public/images');

if (mix.config.inProduction) {
    mix.version();
}

mix.browserSync({
    proxy: 'eliant.app'
});
